package com.semutbulldog.moneysaver.expensemanager.repository;

import com.semutbulldog.moneysaver.expensemanager.model.ExpenseManager;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExpenseManagerRepository extends MongoRepository<ExpenseManager, String> {
    List<ExpenseManager> findExpenseManagerByIdUser(String userId);

    List<ExpenseManager> findExpenseManagerByCategoryAndPeriod(String category, String period);

    ExpenseManager findExpenseManagerById(String id);
}
