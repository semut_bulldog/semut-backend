package com.semutbulldog.moneysaver.expensemanager.controller;

import com.semutbulldog.moneysaver.authorization.security.jwt.JwtUtils;
import com.semutbulldog.moneysaver.expensemanager.model.ExpenseManager;
import com.semutbulldog.moneysaver.expensemanager.model.dto.CreateExpenseManagerDto;
import com.semutbulldog.moneysaver.expensemanager.model.dto.UpdateStartDateDto;
import com.semutbulldog.moneysaver.expensemanager.repository.ExpenseManagerRepository;
import com.semutbulldog.moneysaver.expensemanager.service.ExpenseManagerServiceImpl;
import com.semutbulldog.moneysaver.user.model.User;
import com.semutbulldog.moneysaver.user.repository.UserRepository;
import com.semutbulldog.moneysaver.util.GeneralResponse;
import com.semutbulldog.moneysaver.util.ResourceNotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.OptionalInt;

@RestController
@CrossOrigin(origins = "*")
public class ExpenseManagerController {

    @Autowired
    private ExpenseManagerServiceImpl expenseManagerService;

    @Autowired
    private ExpenseManagerRepository expenseManagerRepository;

    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    private UserRepository userRepository;

    @PostMapping("/expense/manager/create")
    public GeneralResponse<ExpenseManager> createExpenseManager(@RequestBody CreateExpenseManagerDto createExpenseManagerDto) {
        ExpenseManager expenseManager = expenseManagerService.createExpenseManager(createExpenseManagerDto);
        return new GeneralResponse("Success", "Request Successfully Created", expenseManager);
    }

    @GetMapping("/expense/manager/showlist")
    public GeneralResponse<List<ExpenseManager>> showAllExpenseManager() {
        return new GeneralResponse("Success", "Request Successfully Created", expenseManagerRepository.findAll());
    }

    @GetMapping("/expense/manager/{userId}")
    public GeneralResponse<List<ExpenseManager>> getExpenseManagerByUserId(@PathVariable ("userId") String userId) {

        List<ExpenseManager> expenseManagerList = expenseManagerRepository.findExpenseManagerByIdUser(userId);
        return new GeneralResponse("Success","Request Successfully Created", expenseManagerList);

    }

    @DeleteMapping("/expense/manager/delete/{id}")
    public ExpenseManager deleteExpenseManager(@PathVariable String id) {
        ExpenseManager expenseManager = expenseManagerRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFound("not found"));
        expenseManagerRepository.delete(expenseManager);

        return expenseManager;
    }

    @PatchMapping("/expense/manager/date/start/edit/{id}")
    public UpdateStartDateDto editStartDate(@PathVariable("id") String id, @RequestBody UpdateStartDateDto updateStartDateDto) {
        UpdateStartDateDto updateStartDateDto1 = expenseManagerService.updateStartDate(id, updateStartDateDto);
        return updateStartDateDto1;
    }

    @GetMapping("/expense/manager/current")
    public GeneralResponse<Integer> getExpenseManagerByCategoryAndPeriod(@RequestParam String category, String period) {
        List<ExpenseManager> listExpenseManager = expenseManagerRepository.findExpenseManagerByCategoryAndPeriod(category, period);
        Integer total = 0;
        for (ExpenseManager expenseManager : listExpenseManager) {
            total += expenseManager.getNominal();
        }
        return new GeneralResponse("Success", "Request Successfully Created", total);
    }

    @PutMapping("/expense/manager/edit/{id}")
    public GeneralResponse<ExpenseManager> editExpenseManager(
            @PathVariable ("id") String id,
            @RequestBody ExpenseManager expenseManager) {

        ExpenseManager expenseManager1 = expenseManagerService.updateExpenseManager(id, expenseManager);

        GeneralResponse<ExpenseManager>
                response = new GeneralResponse<>("success", "Update data success", expenseManager1);

        return response;
    }

    String getUserIdFromAuthorization(HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        String email = jwtUtils.getUserNameFromJwtToken(token);
        Optional<User> user = userRepository.findByEmail(email);
        return user.get().getId();
    }

}

