package com.semutbulldog.moneysaver.expensemanager.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter @Setter
public class CreateExpenseManagerDto {
    private String id;
    private String idUser;
    private String accountNumber;
    private String accountName;
    private String expenseManagerName;
    @JsonFormat(pattern = "dd/MM/yyyy")
    private Date startDate;
    private String period;
    private Integer nominal;
    private String category;

}
