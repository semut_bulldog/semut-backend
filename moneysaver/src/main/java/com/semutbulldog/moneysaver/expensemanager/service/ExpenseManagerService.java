package com.semutbulldog.moneysaver.expensemanager.service;

import com.semutbulldog.moneysaver.expensemanager.model.ExpenseManager;
import com.semutbulldog.moneysaver.expensemanager.model.dto.CreateExpenseManagerDto;
import com.semutbulldog.moneysaver.expensemanager.model.dto.UpdateNameNominalPeriodeDto;

import java.util.List;

public interface ExpenseManagerService {



    ExpenseManager createExpenseManager(CreateExpenseManagerDto createExpenseManagerDto);

    List<ExpenseManager> getExpenseManagerByUserId(String userId);

    ExpenseManager updateExpenseManager(String id, ExpenseManager expenseManager);

}
