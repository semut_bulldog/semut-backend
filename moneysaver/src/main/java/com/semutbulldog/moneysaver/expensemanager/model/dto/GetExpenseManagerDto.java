package com.semutbulldog.moneysaver.expensemanager.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter @Setter
public class GetExpenseManagerDto {
    private String id;
    private String accountNumber;
    private String accountName;
    private String expenseManagerName;
    @JsonFormat(pattern = "dd/MM/yyyy")
    private Date startDate;
    private String period;
    private Integer nominal;
    private String category;
    private Long currentExpense;
}
