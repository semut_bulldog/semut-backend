package com.semutbulldog.moneysaver.expensemanager.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document("expenseManager")
@Getter @Setter

public class ExpenseManager {
    @Transient public static final String SEQUENCE_NAME = "expense_sequence";
    @Id private String id;
    private String accountNumber;
    private String accountName;
    private String expenseManagerName;
    @JsonFormat(pattern = "dd/MM/yyyy")
    private Date startDate;
    private String period;
    private Integer nominal;
    private String category;
    private String idUser;
    @JsonFormat(pattern = "dd/MM/yyyy") private Date dates;


}
