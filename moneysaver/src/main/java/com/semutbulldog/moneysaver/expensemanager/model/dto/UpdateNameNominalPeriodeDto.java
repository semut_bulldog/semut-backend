package com.semutbulldog.moneysaver.expensemanager.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UpdateNameNominalPeriodeDto {

    private String expenseManagerName;
    private String period;
    private Integer nominal;
}
