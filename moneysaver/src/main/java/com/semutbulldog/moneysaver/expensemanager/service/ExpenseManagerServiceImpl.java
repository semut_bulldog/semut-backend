package com.semutbulldog.moneysaver.expensemanager.service;

import com.semutbulldog.moneysaver.expensemanager.model.ExpenseManager;
import com.semutbulldog.moneysaver.expensemanager.model.dto.CreateExpenseManagerDto;
import com.semutbulldog.moneysaver.expensemanager.model.dto.UpdateStartDateDto;
import com.semutbulldog.moneysaver.expensemanager.repository.ExpenseManagerRepository;
import com.semutbulldog.moneysaver.util.ModelMapperUtil;
import com.semutbulldog.moneysaver.util.NextSequenceService;
import com.semutbulldog.moneysaver.util.ResourceNotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class ExpenseManagerServiceImpl implements ExpenseManagerService {

    @Autowired
    private ExpenseManagerRepository expenseManagerRepository;

    @Autowired
    private ModelMapperUtil modelMapperUtil;

    @Autowired
    private NextSequenceService nextSequenceService;

    @Override
    public ExpenseManager createExpenseManager(CreateExpenseManagerDto createExpenseManagerDto) {
        ExpenseManager expenseManager = new ExpenseManager();
        expenseManager.setId(nextSequenceService.generateSequence(ExpenseManager.SEQUENCE_NAME));
        expenseManager.setIdUser(createExpenseManagerDto.getIdUser());
        expenseManager.setAccountName(createExpenseManagerDto.getAccountName());
        expenseManager.setAccountNumber(createExpenseManagerDto.getAccountNumber());
        expenseManager.setExpenseManagerName(createExpenseManagerDto.getExpenseManagerName());
        expenseManager.setStartDate(createExpenseManagerDto.getStartDate());
        expenseManager.setPeriod(createExpenseManagerDto.getPeriod());
        expenseManager.setNominal(createExpenseManagerDto.getNominal());
        expenseManager.setCategory(createExpenseManagerDto.getCategory());
        expenseManager.setDates(new Date());
        expenseManagerRepository.save(expenseManager);
        return expenseManager;
    }

    @Override
    public List<ExpenseManager> getExpenseManagerByUserId(String userId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(userId));
        return expenseManagerRepository.findExpenseManagerByIdUser(userId);

    }

    public UpdateStartDateDto updateStartDate(String id, UpdateStartDateDto updateStartDateDto) {
        ExpenseManager expenseManager = modelMapperUtil
                .ModelMapperInit()
                .map(updateStartDateDto, ExpenseManager.class);
        ExpenseManager expenseManager1 = expenseManagerRepository.findById(id).orElseThrow(() -> new ResourceNotFound("data not found with id: " + id));
        expenseManager1.setStartDate(expenseManager.getStartDate());
        expenseManagerRepository.save(expenseManager1);
        return updateStartDateDto;
    }

    @Override
    public ExpenseManager updateExpenseManager(String id, ExpenseManager expenseManager) {
        ExpenseManager expenseManager1 = expenseManagerRepository.findExpenseManagerById(id);
        if (expenseManager.getExpenseManagerName() != null) {
            expenseManager1.setExpenseManagerName(expenseManager.getExpenseManagerName());
        }
        if (expenseManager.getNominal() != null) {
            expenseManager1.setNominal(expenseManager.getNominal());
        }
        if (expenseManager.getPeriod() != null) {
            expenseManager1.setPeriod(expenseManager.getPeriod());
        }
        if (expenseManager.getCategory() != null) {
            expenseManager1.setCategory(expenseManager.getCategory());
        }
        expenseManagerRepository.save(expenseManager1);

        return expenseManager1;
    }
}
