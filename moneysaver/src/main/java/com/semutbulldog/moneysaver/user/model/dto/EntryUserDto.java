package com.semutbulldog.moneysaver.user.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.semutbulldog.moneysaver.transaction.model.Transaction;
import com.semutbulldog.moneysaver.user.model.User;

import java.util.List;

public class EntryUserDto {
    @JsonProperty("user")
    public User user;
    @JsonProperty("transaction")
    public List<Transaction> transaction;

    public EntryUserDto(User user, List<Transaction> transaction) {
        this.user = user;
        this.transaction = transaction;
    }
}
