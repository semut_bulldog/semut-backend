package com.semutbulldog.moneysaver.user.repository;

import com.semutbulldog.moneysaver.dashboard.model.dto.DashboardNotificationDto;
import com.semutbulldog.moneysaver.user.model.User;
import com.semutbulldog.moneysaver.user.model.dto.DetailUserBallanceDto;
import org.apache.tomcat.util.buf.UDecoder;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.UnsatisfiedServletRequestParameterException;

import java.util.Optional;
import java.util.List;

@Repository
public interface UserRepository extends MongoRepository<User, String> {
    User findUserModelById(String id);

    Optional<User> findByEmail(String email);

    Boolean existsByEmailAndPassword(String email,String password);

    Boolean existsByEmail(String email);

    Boolean existsByPassword(String password);

    Boolean existsByAccountNumber(String accountNumber);

    Optional<User> findByEmailAndPassword(String email, String password);

    User countAllById();

    User findByAccountNumber(String accountNumber);

    List<User> findAllByIsCheckFalse();

//    List<User> findAll(Sort sort);
//    void save(User user, User user1);
}
