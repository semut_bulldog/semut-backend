package com.semutbulldog.moneysaver.user.service;

import com.semutbulldog.moneysaver.dashboard.model.dto.DashboardNotificationDto;
import com.semutbulldog.moneysaver.user.model.dto.*;
import com.semutbulldog.moneysaver.user.model.User;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface UserService {
    CreateUserDto insertUser(CreateUserDto createUserDto);

    User getUserByIdUser(String id);

    List<DashboardNotificationDto> uncheckedUser();

    UserDetailVerifikasiDto getUserDetailVerifikasi(String id);

    UserStatusDto editUserStatus(String id, UserStatusDto userStatusDto);
    UserVerifiedDto editVerifiedStatus(String id, UserVerifiedDto userVerifiedDto);

    UpdateBalanceDto updateBalanceDto(String accountNumber, UpdateBalanceDto updateBalanceDto);

//    UpdateUserPasswordDto updateUserPassword(String id, UpdateUserPasswordDto updateUserPasswordDto);

    User topUpBalance(String userId, String amount);

    EditDOBDto editDOB(String id, EditDOBDto editDOBDto);

    UpdateUserPinDto updateUserPin(String id, UpdateUserPinDto updateUserPinDto);

    User addUserPhoto(String id, MultipartFile file);

    DetailUserBallanceDto getUserBalanceByAccountNumber(String accountNumber);

}
