package com.semutbulldog.moneysaver.user.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.semutbulldog.moneysaver.transaction.model.Transaction;
import lombok.Getter;
import lombok.Setter;
import org.bson.types.Binary;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class CreateUserDto {

    private String fullName;
    private String email;
    private String phoneNumber;
    @JsonFormat(pattern = "dd/MM/yyyy")
    private Date dateOfBirth;
    private String gender;
    private String address;
    private String nikNumber;
    private String accountNumber;
//    private Binary image;
//    @DBRef private List<Transaction> transactionList;
}