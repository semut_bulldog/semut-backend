package com.semutbulldog.moneysaver.user.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter @Setter
public class UserDetailVerifikasiDto {
    private String fullName;
    private String accountNumber;
    @JsonFormat(pattern = "dd/MM/yyyy")
    private Date dateOfBirth;
    private String nikNumber;
    private String email;
    private String gender;
    private String image;
}
