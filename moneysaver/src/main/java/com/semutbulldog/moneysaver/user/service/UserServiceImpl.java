package com.semutbulldog.moneysaver.user.service;

import com.semutbulldog.moneysaver.dashboard.model.dto.DashboardNotificationDto;
import com.semutbulldog.moneysaver.transaction.model.Transaction;
import com.semutbulldog.moneysaver.transaction.repository.TransactionRepository;
import com.semutbulldog.moneysaver.user.model.dto.*;
import com.semutbulldog.moneysaver.user.model.User;
import com.semutbulldog.moneysaver.user.repository.UserRepository;
import com.semutbulldog.moneysaver.util.AmazonS3Config;
import com.semutbulldog.moneysaver.util.ModelMapperUtil;
import com.semutbulldog.moneysaver.util.ResourceNotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private ModelMapperUtil modelMapperUtil;

    @Autowired
    private AmazonS3Config amazonS3Config;

    @Override
    public CreateUserDto insertUser(CreateUserDto createUserDto) {
        User user = modelMapperUtil
                .ModelMapperInit()
                .map(createUserDto, User.class);
        user.setFullName(createUserDto.getFullName());
        user.setEmail(createUserDto.getEmail());
        user.setPhoneNumber(createUserDto.getPhoneNumber());
        user.setDateOfBirth(createUserDto.getDateOfBirth());
        user.setGender(createUserDto.getGender());
        user.setAddress(createUserDto.getAddress());
        user.setNikNumber(createUserDto.getNikNumber());
        user.setAccountNumber(createUserDto.getAccountNumber());
        userRepository.save(user);

        return createUserDto;
    }

    @Override
    public User getUserByIdUser(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("idUser").is(id));
        return userRepository.findUserModelById(id);
    }

    @Override
    public List<DashboardNotificationDto> uncheckedUser() {
        List<User> user = userRepository.findAllByIsCheckFalse();
        List<DashboardNotificationDto> dashboardNotificationDtoList = new ArrayList<>();
        for (User userData: user) {
            DashboardNotificationDto dashboardNotificationDto = new DashboardNotificationDto();
            dashboardNotificationDto.setId(userData.getId());
            dashboardNotificationDto.setAvatar(userData.getImage());
            dashboardNotificationDto.setTitle(userData.getFullName());
            dashboardNotificationDto.setDatetime(userData.getUpdatedAt());
            dashboardNotificationDtoList.add(dashboardNotificationDto);
        }
        return dashboardNotificationDtoList;
    }

    @Override
    public UserDetailVerifikasiDto getUserDetailVerifikasi(String id) {
        User user = getUserByIdUser(id);
        UserDetailVerifikasiDto userDetailVerifikasiDto = modelMapperUtil
                .ModelMapperInit()
                .map(user, UserDetailVerifikasiDto.class);
        userDetailVerifikasiDto.setFullName(user.getFullName());
        userDetailVerifikasiDto.setAccountNumber(user.getAccountNumber());
        userDetailVerifikasiDto.setDateOfBirth(user.getDateOfBirth());
        userDetailVerifikasiDto.setNikNumber(user.getNikNumber());
        userDetailVerifikasiDto.setEmail(user.getEmail());
        userDetailVerifikasiDto.setGender(user.getGender());
        userDetailVerifikasiDto.setImage(user.getImage());
        return userDetailVerifikasiDto;
    }

    @Override
    public UserStatusDto editUserStatus(String id, UserStatusDto userStatusDto) {
        User user = modelMapperUtil
                .ModelMapperInit()
                .map(userStatusDto, User.class);
        User user1 = getUserByIdUser(id);
        user1.setIsCheck(user.getIsCheck());
        userRepository.save(user1);
        return userStatusDto;
    }

    @Override
    public UserVerifiedDto editVerifiedStatus(String id, UserVerifiedDto userVerifiedDto) {
        User user = modelMapperUtil
                .ModelMapperInit()
                .map(userVerifiedDto, User.class);
        User user1 = getUserByIdUser(id);
        user1.setIsVerified(user.getIsVerified());
        userRepository.save(user1);
        return userVerifiedDto;
    }

    @Override
    public UpdateBalanceDto updateBalanceDto(String accountNumber, UpdateBalanceDto updateBalanceDto) {
        User user = modelMapperUtil
                .ModelMapperInit()
                .map(updateBalanceDto, User.class);
        User user1 = userRepository.findByAccountNumber(accountNumber);
        user1.setUserBalance(user.getUserBalance());
        userRepository.save(user1);
        return updateBalanceDto;
    }

    @Override
    public User topUpBalance(String userId, String amount) {
        User user = userRepository.findById(userId).get();
        user.setUserBalance(user.getUserBalance().add(new BigDecimal(amount)));
        Transaction transaction = new Transaction();
        transaction.setTransactionName("Tabungan");
        transaction.setTransactionType("Tabungan");
        transaction.setNote("");
        transaction.setCreatedAt(new Date());
        transaction.setNominal(new BigDecimal(amount));
        transaction.setCategory("Uang masuk");
        transaction.setAccountName(user.getFullName());
        transaction.setUserId(userId);
        userRepository.save(user);
        transactionRepository.save(transaction);
        return user;
    }

    @Override
    public EditDOBDto editDOB(String id, EditDOBDto editDOBDto) {
        User user = modelMapperUtil
                .ModelMapperInit()
                .map(editDOBDto, User.class);
        User user1 = getUserByIdUser(id);
        user1.setDateOfBirth(user.getDateOfBirth());
        userRepository.save(user1);
        return editDOBDto;
    }

    @Override
    public UpdateUserPinDto updateUserPin(String id, UpdateUserPinDto updateUserPinDto) {
        User user = modelMapperUtil
                .ModelMapperInit()
                .map(updateUserPinDto, User.class);
        User user1 = getUserByIdUser(id);
        user1.setUserPin(user.getUserPin());
        userRepository.save(user1);
        return updateUserPinDto;
    }

    @Override
    public User addUserPhoto(String email, MultipartFile file) {
        String fileUrl = amazonS3Config.uploadFile(file);
        User user1 = userRepository.findByEmail(email).orElseThrow(() -> new ResourceNotFound("email not found"));
        user1.setImage(fileUrl);
        userRepository.save(user1);
        return user1;
    }

    @Override
    public DetailUserBallanceDto getUserBalanceByAccountNumber(String accountNumber) {
        User detaiUser = userRepository.findByAccountNumber(accountNumber);

        DetailUserBallanceDto detailUserBallanceDto = modelMapperUtil
                .ModelMapperInit()
                .map(detaiUser, DetailUserBallanceDto.class);


        detailUserBallanceDto.setAccountNumber(detaiUser.getAccountNumber());
        detailUserBallanceDto.setUserBalance(detaiUser.getUserBalance());

        return detailUserBallanceDto;

    }

}
