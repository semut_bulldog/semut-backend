package com.semutbulldog.moneysaver.user.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.semutbulldog.moneysaver.expensemanager.model.ExpenseManager;
import com.semutbulldog.moneysaver.transaction.model.Transaction;
import com.semutbulldog.moneysaver.user.model.User;

import java.util.List;

public class UserExpenseManagerDto {

    @JsonProperty("user")
    public User user;
    @JsonProperty("expenseManager")
    public List<ExpenseManager> expenseManagers;

    public UserExpenseManagerDto(User user, List<ExpenseManager> expenseManagers) {
        this.user = user;
        this.expenseManagers = expenseManagers;
    }
}
