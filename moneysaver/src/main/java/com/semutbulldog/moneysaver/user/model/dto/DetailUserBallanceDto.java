package com.semutbulldog.moneysaver.user.model.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class DetailUserBallanceDto {

    private String accountNumber;
    private BigDecimal userBalance;

}
