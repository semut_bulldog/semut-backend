package com.semutbulldog.moneysaver.user.controller;

import com.semutbulldog.moneysaver.authorization.security.jwt.JwtUtils;
import com.semutbulldog.moneysaver.expensemanager.model.ExpenseManager;
import com.semutbulldog.moneysaver.expensemanager.service.ExpenseManagerService;
import com.semutbulldog.moneysaver.transaction.model.Transaction;
import com.semutbulldog.moneysaver.transaction.service.TransactionService;
import com.semutbulldog.moneysaver.user.model.User;
import com.semutbulldog.moneysaver.user.model.dto.*;
import com.semutbulldog.moneysaver.user.repository.UserRepository;
import com.semutbulldog.moneysaver.user.service.UserService;
import com.semutbulldog.moneysaver.util.AmazonS3Config;
import com.semutbulldog.moneysaver.util.GeneralResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "*")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private ExpenseManagerService expenseManagerService;


    @Autowired
    JwtUtils jwtUtils;

    private AmazonS3Config amazonS3Config;

    @Autowired
    UserController(AmazonS3Config amazonS3Config) {
        this.amazonS3Config = amazonS3Config;
    }

    @PostMapping("/user/create")
    public CreateUserDto createNewUser(
            @RequestBody CreateUserDto createUserDto) {
        CreateUserDto createUserDto1 = userService.insertUser(createUserDto);
        return createUserDto1;
    }

    @PatchMapping("/user/photos/add")
    public GeneralResponse<User> uploadUserPhoto(@RequestPart("email") String email, @RequestPart(value = "file") MultipartFile file) {
        return new GeneralResponse("Success", "Request Successfully Created", userService.addUserPhoto(email, file));
    }

//    @PostMapping("/upload")
//    public String uploadFile(@RequestPart(value = "file") MultipartFile file) {
//        return this.amazonS3Config.uploadFile(file);
//    }
//
//    @DeleteMapping("/delete")
//    public String deleteFile(@RequestPart(value = "url") String fileUrl) {
//        return this.amazonS3Config.deleteFileFromS3Bucket(fileUrl);
//    }

    @GetMapping("/user")
    public GeneralResponse<List<User>> showAllUser() {
        return new GeneralResponse("Success", "Request Successfully Created", userRepository.findAll(Sort.by(Sort.Direction.ASC, "isCheck")));
    }

    @GetMapping("/test/user/unchecked")
    public List<User> getUncheckedUsers() {
        return userRepository.findAllByIsCheckFalse();
    }

    @GetMapping("/user/detail/{id}")
    public GeneralResponse<UserDetailVerifikasiDto> getUserDetail(@PathVariable("id") String id) {
        return new GeneralResponse("Success", "Request Successfully Created", userService.getUserDetailVerifikasi(id));
    }

    @PutMapping("/user/status/edit/{id}")
    public UserStatusDto editUserStatus(@RequestBody UserStatusDto userStatusDto, @PathVariable("id") String id) {
        UserStatusDto userStatusDto1 = userService.editUserStatus(id, userStatusDto);
        return userStatusDto1;
    }

    @PutMapping("/user/verify/edit/{id}")
    public UserVerifiedDto editVerifiedStatus(@RequestBody UserVerifiedDto userVerifiedDto, @PathVariable("id") String id) {
        UserVerifiedDto userVerifiedDto1 = userService.editVerifiedStatus(id, userVerifiedDto);
        return userVerifiedDto1;
    }

    @PutMapping("/user/balance/update/{accountNumber}")
    public UpdateBalanceDto updateUserBalance(@PathVariable("accountNumber") String accountNumber, @RequestBody UpdateBalanceDto updateBalanceDto) {
        UpdateBalanceDto updateBalanceDto1 = userService.updateBalanceDto(accountNumber, updateBalanceDto);
        return updateBalanceDto1;
    }

    @PutMapping("/user/dob/edit/{id}")
    public EditDOBDto editDOB(@PathVariable("id") String id, @RequestBody EditDOBDto editDOBDto) {
        EditDOBDto editDOBDto1 = userService.editDOB(id, editDOBDto);
        return editDOBDto1;
    }

    @GetMapping("/user/transaction")
    public GeneralResponse<EntryUserDto> getTransactionByUserId(HttpServletRequest request) {
        String id = getUserIdFromAuthorization(request);
        User user = userService.getUserByIdUser(id);
        List<Transaction> transaction = transactionService.getTransactionByUserId(id);
        GeneralResponse<EntryUserDto> response = new GeneralResponse("Success", "Request Successfully Created", new EntryUserDto(user, transaction));
        return response;
    }


//    @PatchMapping("/user/password/update/{id}")
//    public UpdateUserPasswordDto updateUserPassword(@PathVariable("id") String id, @RequestBody UpdateUserPasswordDto updateUserPasswordDto) {
//        UpdateUserPasswordDto updateUserPasswordDto1 = userService.updateUserPassword(id, updateUserPasswordDto);
//        return updateUserPasswordDto1;
//    }

//    @PatchMapping("/user/pin/update/{id}")
//    public UpdateUserPinDto updateUserPin(@PathVariable("id") String id, @RequestBody UpdateUserPinDto updateUserPinDto) {
//        UpdateUserPinDto updateUserPinDto1 = userService.updateUserPin(id, updateUserPinDto);
//        return updateUserPinDto1;
//    }

    @GetMapping("user/balance/detail/{accountNumber}")
    public GeneralResponse<DetailUserBallanceDto> getUserBalanceByAccountNumber(
            @PathVariable ("accountNumber") String accountNumber) {

        DetailUserBallanceDto detailUserBallanceDto = userService.getUserBalanceByAccountNumber(accountNumber);
        GeneralResponse<DetailUserBallanceDto>
                response = new GeneralResponse<>("success", "Request Successfully Created", detailUserBallanceDto);

        return response;
    }

    @PutMapping("user/balance/topup/{userId}")
    GeneralResponse<User> topUpBalance(@PathVariable("userId") String userId, @RequestParam("amount") String amount) {
        return new GeneralResponse<>("Success", "Balance Updated Successfully", userService.topUpBalance(userId, amount));
    }


    String getUserIdFromAuthorization(HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        String email = jwtUtils.getUserNameFromJwtToken(token);
        Optional<User> user = userRepository.findByEmail(email);
        return user.get().getId();
    }
}
