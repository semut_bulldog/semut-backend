package com.semutbulldog.moneysaver.user.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import com.semutbulldog.moneysaver.authorization.model.ERole;
import com.semutbulldog.moneysaver.authorization.model.Role;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
@NoArgsConstructor
@Getter
@Setter
@Document("user_model")
public class User {

    @Id
    private String id;
    private String fullName;
    private String email;

    @JsonIgnore
    private String password;
    @JsonIgnore
    private String userPin;

    @Size(min = 6, max = 13)
    private String phoneNumber;
    @Field
    private BigDecimal userBalance = new BigDecimal(0);
    @JsonFormat(pattern = "dd/MM/yyyy")
    private Date dateOfBirth;
    private String gender;
    private String address;
    private String nikNumber;
    private String accountNumber;
    @LastModifiedDate @JsonFormat(pattern = "dd/MM/yyyy")
    private Date updatedAt;

    @JsonIgnore
    private Set<Role> roles = new HashSet<Role>(){{
        add(new Role(ERole.ROLE_USER));
    }};

    private String image;
    @Field private Boolean isCheck = false;
    @Field private Boolean isVerified = false;

    @PersistenceConstructor
    public User(String email, String fullName, Date dateOfBirth, String gender, String address, String nikNumber, String accountNumber, Date updatedAt ) {
        this.email = email;
        this.fullName = fullName;
        this.dateOfBirth = dateOfBirth;
        this.gender = gender;
        this.address = address;
        this.nikNumber = nikNumber;
        this.accountNumber = accountNumber;
        this.updatedAt = new Date();
    }

    public User(BigDecimal userBalance) {
        this.setUserBalance(userBalance);
    }

    public User(Boolean isCheck, Boolean isVerified) {
        this.isCheck = isCheck;
        this.isVerified = isVerified;
    }


    public User(String email, String phoneNumber) {
        this.email = email;
        this.phoneNumber = phoneNumber;
    }

//    public static BigDecimal getUserBalance() {
//        return userBalance;
//    }
//
//    public static void setUserBalance(BigDecimal userBalance) {
//        User.userBalance = userBalance;
//    }
}
