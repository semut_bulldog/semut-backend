package com.semutbulldog.moneysaver.user.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class UpdateUserPasswordDto {
    private String password;
}
