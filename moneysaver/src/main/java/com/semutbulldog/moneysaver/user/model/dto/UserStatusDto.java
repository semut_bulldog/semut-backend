package com.semutbulldog.moneysaver.user.model.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Field;

@Getter @Setter
public class UserStatusDto {
    protected Boolean isCheck;
}
