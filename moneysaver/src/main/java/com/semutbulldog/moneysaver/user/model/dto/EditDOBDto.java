package com.semutbulldog.moneysaver.user.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter @Setter
public class EditDOBDto {
    @JsonFormat(pattern = "dd/MM/yyyy")
    private Date dateOfBirth;
}
