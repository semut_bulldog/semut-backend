package com.semutbulldog.moneysaver.user.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class UpdateUserPinDto {
    private String userPin;
}
