package com.semutbulldog.moneysaver.transaction.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.semutbulldog.moneysaver.user.model.User;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
public class CreateTransactionDto {


    private String transactionType;
    private String transactionName;
    private String note;
    private BigDecimal nominal;
    private String category;
    private String accountName;
//    @DBRef
//    private User user;
}
