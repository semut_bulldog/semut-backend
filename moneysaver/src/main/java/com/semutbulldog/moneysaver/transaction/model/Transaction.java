package com.semutbulldog.moneysaver.transaction.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.semutbulldog.moneysaver.user.model.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.util.Date;

@NoArgsConstructor
@Getter
@Setter
@Document("transaction")
public class Transaction {

    @Id
    private String id;
    @JsonFormat(pattern="dd-MM-yyyy")
    private Date createdAt;
    private String transactionType;
    private String transactionName;
    private String note;
    private BigDecimal nominal;
    private String category;
    @JsonIgnore
    private String userId;
    @JsonIgnore
    private String password;
//    @DBRef private User user;
    private String accountName;

//    public Transaction(String id_transaction) {
//        this.id_transaction = id_transaction;
//    }


    public Transaction(String password) {
        this.password = password;
    }
}
