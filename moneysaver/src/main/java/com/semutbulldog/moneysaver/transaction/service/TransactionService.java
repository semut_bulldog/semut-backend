package com.semutbulldog.moneysaver.transaction.service;

import com.semutbulldog.moneysaver.transaction.model.dto.CreateTransactionDto;
import com.semutbulldog.moneysaver.transaction.model.Transaction;
import com.semutbulldog.moneysaver.transaction.model.dto.UpdateCategoryDto;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface TransactionService {

    CreateTransactionDto createNewTransaction(CreateTransactionDto createTransactionDto, String userId);

    List<Transaction> getTransactionByUserId(String userId);

    UpdateCategoryDto updateCategory(String id_transaction, UpdateCategoryDto updateCategoryDto);

    List<Transaction> findEntryTransactionByUserId(String userId);

    List<Transaction> findEntryTransactionByUserIdAndCategoryAndCreatedAt(String userId, Date startDate, Date endDate);

    Transaction editCategory(String id, Transaction transaction);

    List<Transaction> findIssuanceTransactionByUserIdAndCategoryAndCreatedAt(String userId, Date startDate, Date endDate);

    List<Transaction> findIssuanceTransactionByUserId(String userId);

    List<Transaction> findIssuanceTransactionByCategoryIn();

    List<Transaction> findEntryTransactionByCategory();

    Optional<Transaction> getTransactionById(String id);

}
