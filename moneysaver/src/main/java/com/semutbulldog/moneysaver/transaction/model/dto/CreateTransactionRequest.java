package com.semutbulldog.moneysaver.transaction.model.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class CreateTransactionRequest {

    private String transactionType;
    private String transactionName;
    private String note;
    private BigDecimal nominal;
    private String category;
    private String accountName;
    private String password;

}
