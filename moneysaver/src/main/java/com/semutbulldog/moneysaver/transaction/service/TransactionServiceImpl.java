package com.semutbulldog.moneysaver.transaction.service;

import com.semutbulldog.moneysaver.transaction.model.dto.UpdateCategoryDto;
import com.semutbulldog.moneysaver.transaction.model.dto.CreateTransactionDto;
import com.semutbulldog.moneysaver.transaction.model.Transaction;
import com.semutbulldog.moneysaver.transaction.repository.TransactionRepository;
import com.semutbulldog.moneysaver.util.ModelMapperUtil;
import com.semutbulldog.moneysaver.util.ResourceNotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class TransactionServiceImpl implements TransactionService {

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private ModelMapperUtil modelMapperUtil;

    @Override
    public CreateTransactionDto createNewTransaction(CreateTransactionDto createTransactionDto, String userId) {
        Transaction transaction = modelMapperUtil
                .ModelMapperInit()
                .map(createTransactionDto, Transaction.class);

        transaction.setTransactionType(createTransactionDto.getTransactionType());
        transaction.setTransactionName(createTransactionDto.getTransactionName());
        transaction.setNote(createTransactionDto.getNote());
        transaction.setCreatedAt(new Date());
        transaction.setNominal(createTransactionDto.getNominal());
        transaction.setCategory(createTransactionDto.getCategory());
        transaction.setAccountName(createTransactionDto.getAccountName());
        transactionRepository.save(transaction);

        return createTransactionDto;
    }

    @Override
    public List<Transaction> getTransactionByUserId(String userId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("userId").is(userId));
        return transactionRepository.findTransaksiByUserId(userId);
    }

    @Override
    public List<Transaction> findEntryTransactionByUserId(String userId) {
        List<String> listTransaction = Arrays.asList("Gaji", "Tabungan", "Hadiah", "Uang masuk");
        return transactionRepository.findEntryTransactionByUserIdAndCategoryIn(userId, listTransaction);

    }

    @Override
    public List<Transaction> findEntryTransactionByUserIdAndCategoryAndCreatedAt(String userId, Date startDate, Date endDate) {
        List<String> listTransaction = Arrays.asList("Gaji", "Tabungan", "Hadiah", "Uang masuk");
        return transactionRepository.findEntryTransactionByUserIdAndCategoryInAndCreatedAtBetween(userId,listTransaction,startDate,endDate);

    }

    @Override
    public List<Transaction> findEntryTransactionByCategory() {
        List<String> listTransaction = Arrays.asList("Gaji", "Tabungan", "Hadiah", "Uang masuk");
        return transactionRepository.findEntryTransactionByCategoryIn(listTransaction);
    }

    @Override
    public Optional<Transaction> getTransactionById(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id_transaction").is(id));
        return transactionRepository.findById(id);
    }

    @Override
    public UpdateCategoryDto updateCategory(String id_transaction, UpdateCategoryDto updateCategoryDto) {
        Transaction transaction = modelMapperUtil
                .ModelMapperInit()
                .map(updateCategoryDto, Transaction.class);
        Transaction transaction1 = transactionRepository.findById(id_transaction).orElseThrow(() -> new ResourceNotFound("not found"));
        transaction1.setCategory(transaction.getCategory());
        transactionRepository.save(transaction1);
        return updateCategoryDto;
    }

    @Override
    public Transaction editCategory(String id, Transaction transaction) {
        Transaction transaction1 = transactionRepository.findById(id).orElseThrow(() -> new ResourceNotFound("not found"));
        if (transaction.getCategory() != null) {
            transaction1.setCategory(transaction.getCategory());
        }
        transactionRepository.save(transaction1);

        return transaction1;
    }

    @Override
    public List<Transaction> findIssuanceTransactionByUserIdAndCategoryAndCreatedAt(String userId, Date startDate, Date endDate) {
        List<String> listTransaction = Arrays.asList("Makanan", "Donasi", "Pendidikan", "Liburan", "Belanja", "Hobi", "Kendaraan", "Internet", "Kesehatan", "E-wallet", "Biaya dan tagihan", "Uang keluar");
        return transactionRepository.findTransactionByUserIdAndCategoryInAndCreatedAtBetween(userId, listTransaction, startDate, endDate);

    }

    @Override
    public List<Transaction> findIssuanceTransactionByUserId(String userId) {
        List<String> listTransaction = Arrays.asList("Makanan", "Donasi", "Pendidikan", "Liburan", "Belanja", "Hobi", "Kendaraan", "Internet", "Kesehatan", "E-wallet", "Biaya dan tagihan", "Uang keluar");
        return transactionRepository.findEntryTransactionByUserIdAndCategoryIn(userId, listTransaction);
    }

    @Override
    public List<Transaction> findIssuanceTransactionByCategoryIn() {
        List<String> listTransaction = Arrays.asList("Makanan", "Donasi", "Pendidikan", "Liburan", "Belanja", "Hobi", "Kendaraan", "Internet", "Kesehatan", "E-wallet", "Biaya dan tagihan", "Uang keluar");
        return transactionRepository.findIssuanceTransactionByCategoryIn(listTransaction);
    }

}
