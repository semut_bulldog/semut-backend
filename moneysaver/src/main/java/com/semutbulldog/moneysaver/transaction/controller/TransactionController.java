package com.semutbulldog.moneysaver.transaction.controller;

import com.semutbulldog.moneysaver.authorization.security.jwt.JwtUtils;
import com.semutbulldog.moneysaver.transaction.model.Transaction;
import com.semutbulldog.moneysaver.transaction.model.dto.CreateTransactionDto;
import com.semutbulldog.moneysaver.transaction.model.dto.CreateTransactionRequest;
import com.semutbulldog.moneysaver.transaction.model.dto.UpdateCategoryDto;
import com.semutbulldog.moneysaver.transaction.repository.TransactionRepository;
import com.semutbulldog.moneysaver.transaction.service.TransactionService;
import com.semutbulldog.moneysaver.user.model.User;
import com.semutbulldog.moneysaver.user.repository.UserRepository;
import com.semutbulldog.moneysaver.util.GeneralResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "*")
public class TransactionController {
    @Autowired
    private TransactionService transactionService;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    PasswordEncoder encoder;


    @PostMapping("/transaction/create")
    public ResponseEntity<?> createNewTransaction(
            @RequestBody CreateTransactionRequest createTransaction, HttpServletRequest request) {

        String userId = getUserIdFromAuthorization(request);
        Optional<User> userPassword = userRepository.findById(userId);

        if (userPassword.isPresent()) {
            String password = userPassword.get().getPassword();
            System.out.println("password " + createTransaction.getCategory() + " lg " + password);
            if (!encoder.matches(createTransaction.getPassword(), password)) {
                return new ResponseEntity<>(new GeneralResponse<String>("Failed", "Password is wrong!", null), HttpStatus.UNAUTHORIZED);
            }
        }


        List<String> listTransaction = Arrays.asList("Gaji", "Tabungan", "Hadiah", "Uang masuk");
        BigDecimal userBalance = userPassword.get().getUserBalance();
        if (listTransaction.contains(createTransaction.getCategory())) {
            userPassword.get().setUserBalance(userBalance.add(createTransaction.getNominal()));
        } else {
            if (userBalance.compareTo(createTransaction.getNominal()) >= 0) {
                userPassword.get().setUserBalance(userBalance.subtract(createTransaction.getNominal()));
            } else {
                return new ResponseEntity<>(new GeneralResponse<String>("Failed", "Balance isn't enough", null), HttpStatus.UNPROCESSABLE_ENTITY);
            }
        }
        userRepository.save(userPassword.get());

        CreateTransactionDto createTransactionDto = new CreateTransactionDto();
        createTransactionDto.setTransactionType(createTransaction.getTransactionType());
        createTransactionDto.setTransactionName(createTransaction.getTransactionName());
        createTransactionDto.setNote(createTransaction.getNote());
        createTransactionDto.setNominal(createTransaction.getNominal());
        createTransactionDto.setCategory(createTransaction.getCategory());
        createTransactionDto.setAccountName(createTransaction.getAccountName());
        CreateTransactionDto createTransactionDto1 = transactionService.createNewTransaction(createTransactionDto, userId);

        return new ResponseEntity<>(new GeneralResponse<CreateTransactionDto>("Success", "Request Successfully Created", createTransactionDto1), HttpStatus.OK);
    }


    @GetMapping("/transaction/showlist")
    public GeneralResponse<List<Transaction>> showAllTransaction() {
        return new GeneralResponse("Success", "Request Successfully Created", transactionRepository.findAll());
    }

    @GetMapping("/transaction/{id_transaction}")
    public GeneralResponse<Transaction> getTransactionById(@PathVariable String id_transaction) {
        return new GeneralResponse("Success", "Request Successfully Created", transactionRepository.findById(id_transaction));
    }

    @PutMapping("/transaction/category/edit/{id_transaction}")
    public UpdateCategoryDto editCategory(@RequestBody UpdateCategoryDto updateCategoryDto, @PathVariable("id_transaction") String id_transaction) {
        UpdateCategoryDto updateCategoryDto1 = transactionService.updateCategory(id_transaction, updateCategoryDto);
        return updateCategoryDto1;
    }

//    @PutMapping("/transaction/edit/{id_transaction}")
//    public GeneralResponse<Transaction> editCategory(@PathVariable("id_transaction") String id, @RequestBody Transaction transaction) {
//        Transaction transaction1 = transactionService.editCategory(id, transaction);
//
//        GeneralResponse<Transaction> response = new GeneralResponse<>("Success", "Data Updated Successfully", transaction1);
//        return response;
//    }

//    @GetMapping("/transaction/showlist")
//    public GeneralResponse<List<Transaction>> getAllTransaction() {
//        return new GeneralResponse("Success", "Request Successfully Created", new TransactionUserDto(transaction, user));
//    }

    @GetMapping("/transaction/entry")
    public GeneralResponse<List<Transaction>> getEntryTransactionByUserId(
            HttpServletRequest request) {
        String userId = getUserIdFromAuthorization(request);
        List<Transaction> transaction = transactionService.findEntryTransactionByUserId(userId);
        GeneralResponse<List<Transaction>> response = new GeneralResponse("Success", "Request Successfully Created", transaction);
        return response;
    }

    @GetMapping("/transaction/entry/date")
    public GeneralResponse<List<Transaction>> getEntryTransactionByUserIdAndCategoryAndCreatedAt(
            HttpServletRequest request,
            @DateTimeFormat(pattern = "dd-MM-yyyy") @RequestParam Date startDate,
            @DateTimeFormat(pattern = "dd-MM-yyyy") @RequestParam Date endDate) {
        String userId = getUserIdFromAuthorization(request);
        List<Transaction> transaction = transactionService.findEntryTransactionByUserIdAndCategoryAndCreatedAt(userId, startDate, endDate);
        GeneralResponse<List<Transaction>> response = new GeneralResponse("Success", "Request Successfully Created", transaction);
        return response;
    }

    @GetMapping("/transaction/entry/showlist")
    public GeneralResponse<Transaction> showAllEntryTransaction() {
        List<Transaction> transactions = transactionService.findEntryTransactionByCategory();
        GeneralResponse<Transaction> response = new GeneralResponse("Success", "Request Successfully Created", transactions);
        return response;
    }

    @GetMapping("/transaction/issuance")
    public GeneralResponse<List<Transaction>> getIssuanceTransactionByIdUser(
            HttpServletRequest request) {
        String userId = getUserIdFromAuthorization(request);
        List<Transaction> transactionList = transactionService.findIssuanceTransactionByUserId(userId);
        GeneralResponse<List<Transaction>> response = new GeneralResponse("Success", "Request Successfully Created", transactionList);
        return response;
    }

    @GetMapping("/transaction/issuance/date")
    public GeneralResponse<List<Transaction>> getIssuanceTransactionByUserIdAndCategoryAndCreatedAt(
            HttpServletRequest request,
            @DateTimeFormat(pattern = "dd-MM-yyyy") @RequestParam Date startDate,
            @DateTimeFormat(pattern = "dd-MM-yyyy") @RequestParam Date endDate) {
        String userId = getUserIdFromAuthorization(request);
        List<Transaction> transactionList = transactionService.findIssuanceTransactionByUserIdAndCategoryAndCreatedAt(userId, startDate, endDate);
        GeneralResponse<List<Transaction>> response = new GeneralResponse("Success", "Request Successfully Created", transactionList);
        return response;
    }

    @GetMapping("/transaction/issuance/showlist")
    public GeneralResponse<Transaction> showAllIssuanceTransaction() {
        List<Transaction> transactions = transactionService.findIssuanceTransactionByCategoryIn();
        GeneralResponse<Transaction> response = new GeneralResponse("Success", "Request Successfully Created", transactions);
        return response;
    }


    String getUserIdFromAuthorization(HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        String email = jwtUtils.getUserNameFromJwtToken(token);
        Optional<User> user = userRepository.findByEmail(email);
        return user.get().getId();
    }

}
