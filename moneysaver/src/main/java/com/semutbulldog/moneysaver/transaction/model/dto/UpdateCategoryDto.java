package com.semutbulldog.moneysaver.transaction.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class UpdateCategoryDto {
    private String category;
}
