package com.semutbulldog.moneysaver.transaction.repository;

import com.semutbulldog.moneysaver.transaction.model.Transaction;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import javax.swing.text.html.Option;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface TransactionRepository extends MongoRepository<Transaction, String> {

    List<Transaction> findTransaksiByUserId(String userId);

    List<Transaction> findEntryTransactionByUserIdAndCategoryIn(String userId, List<String> listCategory);

    List<Transaction> findEntryTransactionByUserIdAndCategoryInAndCreatedAtBetween(String userId, List<String> listCategory, Date startDate, Date endDate);

    List<Transaction> findTransactionByUserIdAndCategoryInAndCreatedAtBetween(String userId, List<String> listCategory, Date startDate, Date endDate);

    List<Transaction> findTransactionByUserIdAndCategoryIn(String userId, List<String> listCategory);

    List<Transaction> findIssuanceTransactionByCategoryIn(List<String> listCategory);

    List<Transaction> findEntryTransactionByCategoryIn(List<String> listCategory);

//    Transaction findTransactionById_transaction(final String id_transaction);
}
