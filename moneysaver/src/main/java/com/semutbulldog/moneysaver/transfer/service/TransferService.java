package com.semutbulldog.moneysaver.transfer.service;

import com.semutbulldog.moneysaver.transfer.model.dto.CreateTransferDto;
import com.semutbulldog.moneysaver.transfer.model.TransferModel;

import java.util.List;
import java.util.Optional;

public interface TransferService {

    CreateTransferDto createNewTransferDto(CreateTransferDto createTransferDto, String userId);

    List<TransferModel> findTransferByUserId(String userId);

    Optional<TransferModel> getTransferById(String id);

    List<TransferModel> findTransferByCategory();

//    CreateTransferDto createNewTransferDto(CreateTransferDtoRequest createTransfer, String userId);

//    List<TransferModel> findTransferByDate (String userId, Date startDate, Date endDate);

}
