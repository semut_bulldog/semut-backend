package com.semutbulldog.moneysaver.transfer.service;

import com.semutbulldog.moneysaver.transaction.model.Transaction;
import com.semutbulldog.moneysaver.transaction.repository.TransactionRepository;
import com.semutbulldog.moneysaver.transfer.model.dto.CreateTransferDto;
import com.semutbulldog.moneysaver.transfer.model.TransferModel;
import com.semutbulldog.moneysaver.transfer.repository.TransferRepository;
import com.semutbulldog.moneysaver.user.model.User;
import com.semutbulldog.moneysaver.user.repository.UserRepository;
import com.semutbulldog.moneysaver.util.ModelMapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class TransferServiceImpl implements TransferService {

    @Autowired
    private TransferRepository transferRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ModelMapperUtil modelMapperUtil;

    @Autowired
    private TransactionRepository transactionRepository;

    @Override
    public CreateTransferDto createNewTransferDto(CreateTransferDto createTransferDto, String userId) {
        TransferModel transferModel = modelMapperUtil
                .ModelMapperInit()
                .map(createTransferDto, TransferModel.class);

        User user = userRepository.findById(createTransferDto.getUserId()).get();
        User userDestination = userRepository.findByAccountNumber(createTransferDto.getAccountNumber());

        transferModel.setCreatedAt(new Date());
        transferModel.setBankPurpose(createTransferDto.getBankPurpose());
        transferModel.setAccountNumber(createTransferDto.getAccountNumber());
        transferModel.setNominal(createTransferDto.getNominal());
        transferModel.setCategory(createTransferDto.getCategory());
        transferModel.setNotes(createTransferDto.getNotes());
        transferModel.setFavourite(createTransferDto.getFavourite());
        transferModel.setUserId(createTransferDto.getUserId());
        transferModel.setAccountName(user.getFullName());
        transferRepository.save(transferModel);

        Transaction destinationTransaction = new Transaction();
        destinationTransaction.setTransactionName(user.getFullName());
        destinationTransaction.setTransactionType("Transfer");
        destinationTransaction.setNote(createTransferDto.getNotes());
        destinationTransaction.setCreatedAt(new Date());
        destinationTransaction.setNominal(createTransferDto.getNominal());
        destinationTransaction.setCategory("Uang masuk");
        destinationTransaction.setAccountName(userDestination.getFullName());
        destinationTransaction.setUserId(userDestination.getId());
        if (createTransferDto.getNotes() == null) {
            destinationTransaction.setNote("");
        }
        if (createTransferDto.getCategory() == null) {
            destinationTransaction.setCategory("Uang masuk");
        }
        transactionRepository.save(destinationTransaction);

        Transaction originTransaction = new Transaction();
        originTransaction.setTransactionName(userDestination.getFullName());
        originTransaction.setTransactionType("Transfer");
        originTransaction.setNote(createTransferDto.getNotes());
        originTransaction.setCreatedAt(new Date());
        originTransaction.setNominal(createTransferDto.getNominal());
        originTransaction.setCategory(createTransferDto.getCategory());
        originTransaction.setAccountName(user.getFullName());
        originTransaction.setUserId(createTransferDto.getUserId());
        if (createTransferDto.getNotes() == null) {
            originTransaction.setNote("");
        }
        if (createTransferDto.getCategory() == null) {
            originTransaction.setCategory("Uang keluar");
        }
        transactionRepository.save(originTransaction);

        if (createTransferDto.getNotes() == null) {
            destinationTransaction.setNote("");
            transferModel.setNotes("");
        }

        userDestination.setUserBalance(userDestination.getUserBalance().add(new BigDecimal(createTransferDto.getNominal().toString())));
        userRepository.save(userDestination);
        return createTransferDto;
    }

    @Override
    public List<TransferModel> findTransferByUserId(String userId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("userId").is(userId));
        return transferRepository.findTransferByUserId(userId);
    }

    @Override
    public Optional<TransferModel> getTransferById(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id_transfer").is(id));
        return transferRepository.findById(id);

    }

    @Override
    public List<TransferModel> findTransferByCategory() {
        List<String> listTransfer = Arrays.asList("Makanan", "Donasi", "Pendidikan", "Liburan", "Belanja", "Hobi", "Kendaraan", "Internet", "Kesehatan", "E-wallet", "Biaya dan tagihan", "Uang keluar");
        return transferRepository.findTransferByCategory(listTransfer);

    }

//    @Override
//    public List<TransferModel> findTransferByDate(String userId, Date startDate, Date endDate) {
//        return transferRepository.findTransferBy2Date(userId, startDate, endDate);
//    }

}
