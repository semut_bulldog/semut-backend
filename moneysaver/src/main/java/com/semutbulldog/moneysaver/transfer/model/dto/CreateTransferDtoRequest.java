package com.semutbulldog.moneysaver.transfer.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.bson.types.ObjectId;

import java.math.BigDecimal;

@Getter
@Setter
public class CreateTransferDtoRequest {
    @JsonFormat(pattern="dd/MM/yyyy")
    private String userId;
    private String bankPurpose;
    private String accountNumber;
    private BigDecimal nominal;
    private String category;
    private String notes;
    private Boolean favourite;
    private String password;

}
