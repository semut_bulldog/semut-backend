package com.semutbulldog.moneysaver.transfer.controller;

import com.semutbulldog.moneysaver.transfer.model.dto.CreateTransferDto;
import com.semutbulldog.moneysaver.transfer.model.TransferModel;
import com.semutbulldog.moneysaver.transfer.model.dto.CreateTransferDtoRequest;
import com.semutbulldog.moneysaver.transfer.repository.TransferRepository;
import com.semutbulldog.moneysaver.transfer.service.TransferService;
import com.semutbulldog.moneysaver.user.model.User;
import com.semutbulldog.moneysaver.user.model.dto.GetUserNameDto;
import com.semutbulldog.moneysaver.user.repository.UserRepository;
import com.semutbulldog.moneysaver.util.GeneralResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@RestController
//@RequestMapping("/transfer")
@CrossOrigin(origins = "*")

public class TransferController {

    @Autowired
    private TransferService transferService;

    @Autowired
    private TransferRepository transferRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    PasswordEncoder encoder;


    @GetMapping("/transfer/check/{accountNumber}")
    public ResponseEntity<?> checkTransferDestination(@PathVariable String accountNumber, GetUserNameDto getUserNameDto) {
        User user = userRepository.findByAccountNumber(accountNumber);
        getUserNameDto.setFullName(user.getFullName());
        String username = getUserNameDto.getFullName();
        if (username != null) {
            return new ResponseEntity<>(new GeneralResponse<>("Success", "Request Successfully Created", username), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new GeneralResponse<>("Failed", "User doesn't exist", null), HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @PostMapping("/transfer/create")
    public ResponseEntity<?> createNewTransfer(@RequestBody CreateTransferDtoRequest createTransfer, String userId) {
        Optional<User> userPassword = userRepository.findById(createTransfer.getUserId());

        if (userPassword.isPresent()) {
            String password = userPassword.get().getPassword();

            System.out.println("password " + createTransfer.getCategory() + " lg " + password);
            if (!encoder.matches(createTransfer.getPassword(), password)) {
                return new ResponseEntity<>(new GeneralResponse<String>("Failed", "Password is wrong!", null), HttpStatus.UNAUTHORIZED);
            }
        }

        BigDecimal userBalance = userPassword.get().getUserBalance();
        System.out.println(userBalance);

        if (userBalance.compareTo(createTransfer.getNominal()) < 0) {
            System.out.println("nominal " + createTransfer.getNominal());
            return new ResponseEntity<>(new GeneralResponse<String>("Failed", "Balance isn't enough", null), HttpStatus.UNPROCESSABLE_ENTITY);
        }

        System.out.println(userBalance.compareTo(createTransfer.getNominal()) >= 0);
        if (userBalance.compareTo(createTransfer.getNominal()) >= 0) {
            userPassword.get().setUserBalance(userBalance.subtract(createTransfer.getNominal()));
        }

        userRepository.save(userPassword.get());

        Random random = new Random();
        Integer num = 100000000 + random.nextInt(899999999);

        CreateTransferDto createTransferDto = new CreateTransferDto();
        createTransferDto.setUserId(createTransfer.getUserId());
        createTransferDto.setTransactionIdentifier(num);
        createTransferDto.setAccountNumber(createTransfer.getAccountNumber());
        createTransferDto.setNominal(createTransfer.getNominal());
        createTransferDto.setBankPurpose(createTransfer.getBankPurpose());
        createTransferDto.setNotes(createTransfer.getNotes());
        createTransferDto.setCategory(createTransfer.getCategory());
        createTransferDto.setFavourite(createTransfer.getFavourite());
        if (createTransfer.getNotes() == null) {
            createTransferDto.setNotes("");
        }
        CreateTransferDto createTransactionDto1 = transferService.createNewTransferDto(createTransferDto, userId);

        if (userRepository.existsByAccountNumber(createTransfer.getAccountNumber())) {
            return new ResponseEntity<>(new GeneralResponse<>("Success", "Request Successfully Created", createTransactionDto1), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new GeneralResponse<>("Failed", "Account destination doesn't exist!", null), HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @GetMapping("/transfer/showlist")
    public GeneralResponse<List<TransferModel>> showAllTransfer() {
        return new GeneralResponse("Success", "Request Created Successfully", transferRepository.findAll());
    }

    @GetMapping("/transfer/{id_transfer}")
    public GeneralResponse<TransferModel> getTransferById(@PathVariable String id_transfer) {
        return new GeneralResponse("Success", "Request Created Successfully", transferRepository.findById(id_transfer));
    }

//    @GetMapping("/transfer/{userId}")
//    public GeneralResponse<TransferModel> getTransferByUserId(@PathVariable String userId) {
//        return new GeneralResponse("Success", "Request Created Successfully", transferRepository.findTransferByUserId(userId));
//    }

//    @GetMapping("/date")
//    public GeneralResponse<List<TransferModel>> getTransferByDate(
//            @RequestParam String userId,
//            @DateTimeFormat(pattern = "dd-MM-yyyy") @RequestParam Date startDate,
//            @DateTimeFormat(pattern = "dd-MM-yyyy") @RequestParam Date endDate) {
//        List<TransferModel> transferModels = transferService.findTransferByDate(userId, startDate, endDate);
//        GeneralResponse<List<TransferModel>> response = new GeneralResponse("Success", "Request Found Successfully", transferModels);
//        return response;
//    }

    @GetMapping("transfer/category")
    public GeneralResponse<TransferModel> getTransferByCategory() {
        List<TransferModel> transferModels = transferService.findTransferByCategory();
        GeneralResponse<TransferModel> response = new GeneralResponse("Success", "Request Created Successfully", transferModels);
        return response;
    }

}
