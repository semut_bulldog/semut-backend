package com.semutbulldog.moneysaver.transfer.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@Document("transfer")
@NoArgsConstructor
public class TransferModel {

    @Id
    private String id;
    @JsonFormat(pattern="dd-MM-yyyy")
    private String bankPurpose;
    private String accountNumber;
    private BigDecimal nominal;
    private String category;
    private String notes;
    @Field
    private Boolean favourite = false;
    private Date createdAt;
    @JsonIgnore
    private String userId;
    @JsonIgnore
    private String password;

    private String accountName;

//    public TransferModel(String password) {
//        this.password = password;
//    }

}
