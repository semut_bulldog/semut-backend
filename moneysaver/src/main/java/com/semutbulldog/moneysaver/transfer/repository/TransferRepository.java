package com.semutbulldog.moneysaver.transfer.repository;

import com.semutbulldog.moneysaver.transfer.model.TransferModel;
import com.semutbulldog.moneysaver.transfer.model.dto.CreateTransferDto;
import com.semutbulldog.moneysaver.transfer.model.dto.CreateTransferDtoRequest;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface TransferRepository extends MongoRepository<TransferModel, String > {

    List<TransferModel> findTransferById(String id);

    List<TransferModel> findTransferByCategory(List<String> listCategory);

    List<TransferModel> findTransferByUserId(String idUser);

//    List<TransferModel> findTransferBy2Date(String userId, Date startDate, Date endDate);

}
