package com.semutbulldog.moneysaver.transfer.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
//import com.fasterxml.jackson.databind.PropertyNamingStrategy;
//import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import lombok.Setter;
import org.bson.types.ObjectId;

import java.math.BigDecimal;
//import org.springframework.data.annotation.Id;

//import java.util.Date;

@Getter
@Setter
public class CreateTransferDto {

    @JsonFormat(pattern="dd/MM/yyyy")
    private String userId;
    private Integer transactionIdentifier;
    private String bankPurpose;
    private String accountNumber;
    private BigDecimal nominal;
    private String category;
    private String notes;
    private Boolean favourite;
}
