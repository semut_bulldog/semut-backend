package com.semutbulldog.moneysaver.rbac.model.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter @Setter
public class LoginDto {
    @NotBlank
    private String accountUsername;

    @NotBlank
    private String accountPassword;
}
