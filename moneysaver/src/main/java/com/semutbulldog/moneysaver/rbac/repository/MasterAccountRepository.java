package com.semutbulldog.moneysaver.rbac.repository;

import com.semutbulldog.moneysaver.rbac.model.MasterAccount;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MasterAccountRepository extends MongoRepository<MasterAccount, String> {
    MasterAccount findMasterAccountById(String id);

    Optional<MasterAccount> findMasterAccountByAccountUsername(String accountUsername);

    Boolean existsByAccountUsername(String accountUsername);

    Boolean existsByAccountEmail(String accountEmail);
}
