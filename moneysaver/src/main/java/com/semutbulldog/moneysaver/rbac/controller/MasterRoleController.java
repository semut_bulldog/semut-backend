//package com.semutbulldog.moneysaver.rbac.controller;
//
//import com.semutbulldog.moneysaver.rbac.model.MasterRole;
//import com.semutbulldog.moneysaver.rbac.model.dto.CreateMasterRoleDto;
//import com.semutbulldog.moneysaver.rbac.repository.MasterRoleRepository;
//import com.semutbulldog.moneysaver.rbac.service.MasterRoleServiceImpl;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.List;
//
//@RestController
//@CrossOrigin(origins = "*")
//
//public class MasterRoleController {
//
//    @Autowired
//    private MasterRoleServiceImpl masterRoleService;
//
//    @Autowired
//    private MasterRoleRepository masterRoleRepository;
//
//    @PostMapping("/master/role/create")
//    public CreateMasterRoleDto createMasterRole(@RequestBody CreateMasterRoleDto createMasterRoleDto) {
//        CreateMasterRoleDto createMasterRoleDto1 = masterRoleService.createMasterRole(createMasterRoleDto);
//        return createMasterRoleDto1;
//    }
//
//    @GetMapping("/master/role/showlist")
//    public List<MasterRole> showAllMasterRole() {
//        return masterRoleRepository.findAll();
//    }
//}
