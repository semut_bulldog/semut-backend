package com.semutbulldog.moneysaver.rbac.service;

import com.semutbulldog.moneysaver.rbac.model.dto.CreateSubmenuDto;

public interface SubmenuService {
    CreateSubmenuDto createSubmenu(CreateSubmenuDto createSubmenuDto);
}
