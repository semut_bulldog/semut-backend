package com.semutbulldog.moneysaver.rbac.service;

import com.semutbulldog.moneysaver.rbac.model.MasterAccount;
import com.semutbulldog.moneysaver.rbac.model.dto.*;
import com.semutbulldog.moneysaver.rbac.repository.MasterAccountRepository;
import com.semutbulldog.moneysaver.util.ModelMapperUtil;
import com.semutbulldog.moneysaver.util.NextSequenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MasterAccountServiceImpl implements MasterAccountService {

    @Autowired
    private MasterAccountRepository masterAccountRepository;

    @Autowired
    private ModelMapperUtil modelMapperUtil;

    @Autowired
    private NextSequenceService nextSequenceService;

    @Autowired
    PasswordEncoder encoder;

    @Override
    public CreateMasterAccountDto createMasterAccount(CreateMasterAccountDto createMasterAccountDto) {
        MasterAccount masterAccount = modelMapperUtil
                .ModelMapperInit()
                .map(createMasterAccountDto, MasterAccount.class);

        masterAccount.setId(nextSequenceService.generateSequence(MasterAccount.SEQUENCE_NAME));
        masterAccount.setAccountName(createMasterAccountDto.getAccountName());
        masterAccount.setAccountEmail(createMasterAccountDto.getAccountEmail());
        masterAccount.setAccountUsername(createMasterAccountDto.getAccountUsername());
        masterAccount.setAccountPassword(encoder.encode(createMasterAccountDto.getAccountPassword()));
        masterAccountRepository.save(masterAccount);
        return createMasterAccountDto;
    }

    @Override
    public List<AccountDto> showAccounts() {
        List<MasterAccount> masterAccountList = masterAccountRepository.findAll();
        List<AccountDto> accountDtoList = new ArrayList<>();

        for (MasterAccount masterAccount: masterAccountList) {
            AccountDto accountDto = modelMapperUtil
                    .ModelMapperInit()
                    .map(masterAccount, AccountDto.class);

            accountDto.setId(masterAccount.getId());
            accountDto.setAccountUsername(masterAccount.getAccountUsername());
            accountDto.setAccountPassword(masterAccount.getAccountPassword());
            accountDto.setRoles(masterAccount.getRoles());
            accountDtoList.add(accountDto);
        }
        return accountDtoList;
    }

    public MasterAccount getMasterAccountById(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        return masterAccountRepository.findMasterAccountById(id);
    }

    @Override
    public MasterAccount editMasterAccount(String id, MasterAccount masterAccount) {
        MasterAccount masterAccount1 = masterAccountRepository.findMasterAccountById(id);
        if (masterAccount.getAccountName() != null) {
            masterAccount1.setAccountName(masterAccount.getAccountName());
        }
        if (masterAccount.getAccountEmail() != null) {
            masterAccount1.setAccountEmail(masterAccount.getAccountEmail());
        }
        if (masterAccount.getAccountUsername() != null) {
            masterAccount1.setAccountUsername(masterAccount.getAccountUsername());
        }
        masterAccountRepository.save(masterAccount1);
        return masterAccount1;
    }

    @Override
    public UpdateAccountNameDto editAccountName(String id, UpdateAccountNameDto updateAccountNameDto) {
        MasterAccount masterAccount = modelMapperUtil
                .ModelMapperInit()
                .map(updateAccountNameDto, MasterAccount.class);
        MasterAccount masterAccount1 = getMasterAccountById(id);
        masterAccount1.setAccountName(masterAccount.getAccountName());
        masterAccountRepository.save(masterAccount1);
        return updateAccountNameDto;
    }

    @Override
    public UpdateAccountUsernameDto editAccountUsername(String id, UpdateAccountUsernameDto updateAccountUsernameDto) {
        MasterAccount masterAccount = modelMapperUtil
                .ModelMapperInit()
                .map(updateAccountUsernameDto, MasterAccount.class);
        MasterAccount masterAccount1 = getMasterAccountById(id);
        masterAccount1.setAccountUsername(masterAccount.getAccountUsername());
        masterAccountRepository.save(masterAccount1);
        return updateAccountUsernameDto;
    }

    @Override
    public UpdateAccountEmailDto editAccountEmail(String id, UpdateAccountEmailDto updateAccountEmailDto) {
        MasterAccount masterAccount = modelMapperUtil
                .ModelMapperInit()
                .map(updateAccountEmailDto, MasterAccount.class);
        MasterAccount masterAccount1 = getMasterAccountById(id);
        masterAccount1.setAccountEmail(masterAccount.getAccountEmail());
        masterAccountRepository.save(masterAccount1);
        return updateAccountEmailDto;
    }

}
