//package com.semutbulldog.moneysaver.rbac.security;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.core.userdetails.User;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.provisioning.InMemoryUserDetailsManager;
//
//@Configuration
//@EnableWebSecurity
//public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
//    @Override
//    protected void configure(HttpSecurity httpSecurity) throws Exception {
//        httpSecurity
//                .csrf().disable()
//                .authorizeRequests()
////                .antMatchers("/aturpengeluaran/show").access("hasRole('super_admin') or hasRole('admin')")
////                .antMatchers("/aturpengeluaran/delete").access("hasRole('super_admin')")
//                .anyRequest().permitAll()
//                .and()
//                .httpBasic();
//    }
//
////    @Autowired
////    public void configureGlobal(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
////        authenticationManagerBuilder.inMemoryAuthentication()
////                .withUser("dimas").password("bukandimas").authorities("super_admin")
////                .and()
////                .withUser("admin").password("adminbiasa").authorities("admin");
////    }
//
//    @Bean
//    @Override
//    public UserDetailsService userDetailsService() {
//        UserDetails userDetails =
//                User.withDefaultPasswordEncoder()
//                        .username("dimas")
//                        .password("bukandimas")
//                        .roles("ADMIN")
//                        .build();
//
//        return new InMemoryUserDetailsManager(userDetails);
//    }
//}
