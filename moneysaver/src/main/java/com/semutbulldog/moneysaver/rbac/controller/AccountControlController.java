//package com.semutbulldog.moneysaver.rbac.controller;
//
//import com.semutbulldog.moneysaver.rbac.model.AccountControlList;
//import com.semutbulldog.moneysaver.rbac.model.dto.CreateAccountControlDto;
//import com.semutbulldog.moneysaver.rbac.repository.AccountControlRepository;
//import com.semutbulldog.moneysaver.rbac.service.AccountControlService;
//import com.semutbulldog.moneysaver.rbac.service.AccountControlServiceImpl;
//import com.semutbulldog.moneysaver.util.GeneralResponse;
//import com.semutbulldog.moneysaver.util.ResourceNotFound;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.List;
//
//@RestController
//@CrossOrigin(origins = "*")
//public class AccountControlController {
//
//    @Autowired
//    private AccountControlServiceImpl accountControlService;
//
//    @Autowired
//    private AccountControlRepository accountControlRepository;
//
//    @PostMapping("/accountcontrol/create")
//    public CreateAccountControlDto createAccountControl(@RequestBody CreateAccountControlDto createAccountControlDto) {
//        CreateAccountControlDto createAccountControlDto1 = accountControlService.createAccountControl(createAccountControlDto);
//        return createAccountControlDto1;
//    }
//
//    @GetMapping("/accountcontrol/showlist")
//    public GeneralResponse<List<AccountControlList>> showAllAccountControl() {
//        GeneralResponse<List<AccountControlList>> response = new GeneralResponse("Success", "Request Successfully Created", accountControlRepository.findAll());
//        return response;
//    }
//
//    @DeleteMapping("/accountcontrol/delete/{account_control_id}")
//    public AccountControlList deleteAccountControl(@PathVariable String account_control_id) {
//        AccountControlList accountControlList = accountControlRepository.findById(account_control_id)
//                .orElseThrow(() -> new ResourceNotFound("not found"));
//        accountControlRepository.delete(accountControlList);
//
//        return accountControlList;
//    }
//
//}
