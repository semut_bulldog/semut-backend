package com.semutbulldog.moneysaver.rbac.service;

import com.semutbulldog.moneysaver.rbac.model.dto.CreateAccountControlDto;

public interface AccountControlService {
    CreateAccountControlDto createAccountControl(CreateAccountControlDto createAccountControlDto);
}
