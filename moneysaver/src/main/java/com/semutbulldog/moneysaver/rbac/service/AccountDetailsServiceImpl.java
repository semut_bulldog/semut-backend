package com.semutbulldog.moneysaver.rbac.service;

import com.semutbulldog.moneysaver.rbac.model.MasterAccount;
import com.semutbulldog.moneysaver.rbac.repository.MasterAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AccountDetailsServiceImpl implements UserDetailsService {

    @Autowired
    MasterAccountRepository masterAccountRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        MasterAccount masterAccount = masterAccountRepository.findMasterAccountByAccountUsername(username).orElseThrow(() -> new UsernameNotFoundException("User not found with username: " + username));

        return AccountDetailsImpl.build(masterAccount);
    }
}
