package com.semutbulldog.moneysaver.rbac.service;

import com.semutbulldog.moneysaver.rbac.model.dto.CreateMasterRoleDto;

public interface MasterRoleService {
    CreateMasterRoleDto createMasterRole(CreateMasterRoleDto createMasterRoleDto);
}
