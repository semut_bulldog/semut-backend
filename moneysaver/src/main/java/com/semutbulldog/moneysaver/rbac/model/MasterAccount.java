package com.semutbulldog.moneysaver.rbac.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.semutbulldog.moneysaver.authorization.model.ERole;
import com.semutbulldog.moneysaver.authorization.model.Role;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Document("masterAccount")
@Getter @Setter

public class MasterAccount {
    @Transient public static final String SEQUENCE_NAME = "accountSequence";
    @Id private String id;
    private String accountName;
    private String accountEmail;
    private String accountUsername;
    private String accountPassword;
    private Set<Role> roles = new HashSet<Role>(){{
        add(new Role(ERole.OPERATOR));
    }};
}
