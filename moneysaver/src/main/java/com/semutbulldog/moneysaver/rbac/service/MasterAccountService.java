package com.semutbulldog.moneysaver.rbac.service;

import com.semutbulldog.moneysaver.rbac.model.MasterAccount;
import com.semutbulldog.moneysaver.rbac.model.dto.*;

import java.util.List;

public interface MasterAccountService {
    CreateMasterAccountDto createMasterAccount(CreateMasterAccountDto createMasterAccountDto);

    List<AccountDto> showAccounts();

    MasterAccount editMasterAccount(String id, MasterAccount masterAccount);

    UpdateAccountNameDto editAccountName(String id, UpdateAccountNameDto updateAccountNameDto);

    UpdateAccountUsernameDto editAccountUsername(String id, UpdateAccountUsernameDto updateAccountUsernameDto);

    UpdateAccountEmailDto editAccountEmail(String id, UpdateAccountEmailDto updateAccountEmailDto);
}
