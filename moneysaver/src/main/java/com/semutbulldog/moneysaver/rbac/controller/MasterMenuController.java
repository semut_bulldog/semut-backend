//package com.semutbulldog.moneysaver.rbac.controller;
//
//import com.semutbulldog.moneysaver.rbac.model.MasterMenu;
//import com.semutbulldog.moneysaver.rbac.model.dto.CreateMasterMenuDto;
//import com.semutbulldog.moneysaver.rbac.repository.MasterMenuRepository;
//import com.semutbulldog.moneysaver.rbac.service.MasterMenuServiceImpl;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.List;
//
//@RestController
//@CrossOrigin(origins = "*")
//
//public class MasterMenuController {
//
//    @Autowired
//    private MasterMenuServiceImpl masterMenuService;
//
//    @Autowired
//    private MasterMenuRepository masterMenuRepository;
//
//    @PostMapping("/master/menu/create")
//    public CreateMasterMenuDto createMasterMenuDto(@RequestBody CreateMasterMenuDto createMasterMenuDto) {
//        CreateMasterMenuDto createMasterMenuDto1 = masterMenuService.createMasterMenu(createMasterMenuDto);
//        return createMasterMenuDto1;
//    }
//
//    @GetMapping("/master/menu/showlist")
//    public List<MasterMenu> allMasterMenu() {
//        return masterMenuRepository.findAll();
//    }
//
//}
