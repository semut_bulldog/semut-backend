package com.semutbulldog.moneysaver.rbac.service;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.semutbulldog.moneysaver.rbac.model.MasterAccount;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class AccountDetailsImpl implements UserDetails {

    private static final long serialVersionUID = 1L;

    private String id;
    private String accountUsername;
    @JsonIgnore
    private String accountPassword;
    private String accountEmail;

    private Collection<? extends  GrantedAuthority> authorities;

    public AccountDetailsImpl(String id, String accountUsername, String accountPassword, String accountEmail, Collection<? extends GrantedAuthority> authorities) {
        this.id = id;
        this.accountUsername = accountUsername;
        this.accountPassword = accountPassword;
        this.accountEmail = accountEmail;
        this.authorities = authorities;
    }

    public static AccountDetailsImpl build(MasterAccount masterAccount) {
        List<GrantedAuthority> authorities = masterAccount.getRoles().stream()
                .map(role -> new SimpleGrantedAuthority(role.getName().name()))
                .collect(Collectors.toList());

        return new AccountDetailsImpl(
                masterAccount.getId(),
                masterAccount.getAccountUsername(),
                masterAccount.getAccountPassword(),
                masterAccount.getAccountEmail(),
                authorities
        );
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public String getId() {
        return id;
    }

    @Override
    public String getPassword() {
        return accountPassword;
    }

    @Override
    public String getUsername() {
        return accountUsername;
    }

    public String getAccountEmail() {
        return accountEmail;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        AccountDetailsImpl account = (AccountDetailsImpl) o;
        return Objects.equals(id, account.id);
    }
}
