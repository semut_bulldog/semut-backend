package com.semutbulldog.moneysaver.rbac.model.dto;

import com.semutbulldog.moneysaver.authorization.model.Role;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@Setter
public class AccountJwtResponse {
    private String token;
    private String type = "Bearer";
    private String id;
    private String accountUsername;
    private String accountEmail;
    private List<String> roles;

    public AccountJwtResponse(String token, String id, String accountUsername, String accountEmail, List<String> roles) {
        this.token = token;
        this.id = id;
        this.accountUsername = accountUsername;
        this.accountEmail = accountEmail;
        this.roles = roles;
    }
}
