package com.semutbulldog.moneysaver.rbac.model.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Getter @Setter
public class CreateMasterAccountDto {
    private String accountName;
    private String accountEmail;
    private String accountUsername;
    private String accountPassword;

//    private Set<String> roles;
//
//    public CreateMasterAccountDto(){
//        roles = new HashSet<>(Arrays.asList("ADMIN"));
//    }
}
