package com.semutbulldog.moneysaver.rbac.model.dto;

import com.semutbulldog.moneysaver.rbac.model.SubMenu;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.util.List;

@Getter @Setter
public class CreateMasterMenuDto {
    private String menuName;
    @DBRef private List<SubMenu> subMenu;
}
