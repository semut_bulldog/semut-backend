package com.semutbulldog.moneysaver.rbac.repository;

import com.semutbulldog.moneysaver.rbac.model.AccountControlList;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountControlRepository extends MongoRepository<AccountControlList, String > {
}
