package com.semutbulldog.moneysaver.rbac.model.dto;

import com.semutbulldog.moneysaver.rbac.model.MasterAccount;
import com.semutbulldog.moneysaver.rbac.model.MasterMenu;
import com.semutbulldog.moneysaver.rbac.model.MasterRole;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.util.List;

@Getter @Setter
public class CreateAccountControlDto {
    @DBRef private List<MasterAccount> account;
    @DBRef private List<MasterRole> role;
//    @DBRef private List<MasterMenu> menu;
}
