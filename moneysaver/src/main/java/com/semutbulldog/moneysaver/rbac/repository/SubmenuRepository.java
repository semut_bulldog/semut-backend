package com.semutbulldog.moneysaver.rbac.repository;

import com.semutbulldog.moneysaver.rbac.model.SubMenu;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SubmenuRepository extends MongoRepository<SubMenu, String> {
}
