package com.semutbulldog.moneysaver.rbac.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("master_role")
@Getter @Setter
public class MasterRole {
    @Transient public static final String SEQUENCE_NAME = "roleSequence";
    @Id private String id;
    private String role;
}
