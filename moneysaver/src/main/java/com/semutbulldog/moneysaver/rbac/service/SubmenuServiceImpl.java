package com.semutbulldog.moneysaver.rbac.service;

import com.semutbulldog.moneysaver.rbac.model.SubMenu;
import com.semutbulldog.moneysaver.rbac.model.dto.CreateSubmenuDto;
import com.semutbulldog.moneysaver.rbac.repository.SubmenuRepository;
import com.semutbulldog.moneysaver.util.ModelMapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SubmenuServiceImpl implements SubmenuService {

    @Autowired
    private SubmenuRepository submenuRepository;

    @Autowired
    private ModelMapperUtil modelMapperUtil;

    @Override
    public CreateSubmenuDto createSubmenu(CreateSubmenuDto createSubmenuDto) {
        SubMenu subMenu = modelMapperUtil
                .ModelMapperInit()
                .map(createSubmenuDto, SubMenu.class);

        subMenu.setSubmenuName(createSubmenuDto.getSubmenuName());
        submenuRepository.save(subMenu);

        return createSubmenuDto;
    }
}
