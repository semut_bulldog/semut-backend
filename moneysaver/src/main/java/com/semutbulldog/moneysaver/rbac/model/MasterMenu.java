package com.semutbulldog.moneysaver.rbac.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Getter @Setter
@Document("master_menu")
public class MasterMenu {
    @Id private String idMenu;
    private String menuName;
    @DBRef private List<SubMenu> subMenu;
}
