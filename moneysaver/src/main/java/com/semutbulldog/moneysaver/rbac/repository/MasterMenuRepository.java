package com.semutbulldog.moneysaver.rbac.repository;

import com.semutbulldog.moneysaver.rbac.model.MasterMenu;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MasterMenuRepository extends MongoRepository<MasterMenu, String> {
}
