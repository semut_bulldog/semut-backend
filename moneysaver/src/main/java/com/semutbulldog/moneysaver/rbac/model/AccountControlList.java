package com.semutbulldog.moneysaver.rbac.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document("account_control_list")
@Getter @Setter

public class AccountControlList {
    @Transient public static final String SEQUENCE_NAME = "accountControlSequence";
    @Id private String id;
    @DBRef private List<MasterAccount> account;
    @DBRef private List<MasterRole> role;
//    @DBRef private List<MasterMenu> menu;
}
