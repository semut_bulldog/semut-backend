package com.semutbulldog.moneysaver.rbac.model.dto;

import com.semutbulldog.moneysaver.authorization.model.Role;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter @Setter
public class AccountDto {
    private String id;
    private String accountUsername;
    private String accountPassword;
    private Set<Role> roles;
}
