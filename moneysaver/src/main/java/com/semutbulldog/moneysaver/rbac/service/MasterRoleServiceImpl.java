package com.semutbulldog.moneysaver.rbac.service;

import com.semutbulldog.moneysaver.rbac.model.MasterRole;
import com.semutbulldog.moneysaver.rbac.model.dto.CreateMasterRoleDto;
import com.semutbulldog.moneysaver.rbac.repository.MasterRoleRepository;
import com.semutbulldog.moneysaver.util.ModelMapperUtil;
import com.semutbulldog.moneysaver.util.NextSequenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MasterRoleServiceImpl implements MasterRoleService {

    @Autowired
    private MasterRoleRepository masterRoleRepository;

    @Autowired
    private ModelMapperUtil modelMapperUtil;

    @Autowired
    private NextSequenceService nextSequenceService;

    @Override
    public CreateMasterRoleDto createMasterRole(CreateMasterRoleDto createMasterRoleDto) {
        MasterRole masterRole = modelMapperUtil
                .ModelMapperInit()
                .map(createMasterRoleDto, MasterRole.class);

        masterRole.setId(nextSequenceService.generateSequence(MasterRole.SEQUENCE_NAME));
        masterRole.setRole(createMasterRoleDto.getRole());
        masterRoleRepository.save(masterRole);
        return createMasterRoleDto;
    }
}
