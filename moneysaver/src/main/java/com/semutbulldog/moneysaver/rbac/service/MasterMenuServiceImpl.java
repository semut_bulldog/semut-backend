package com.semutbulldog.moneysaver.rbac.service;

import com.semutbulldog.moneysaver.rbac.model.MasterMenu;
import com.semutbulldog.moneysaver.rbac.model.dto.CreateMasterMenuDto;
import com.semutbulldog.moneysaver.rbac.repository.MasterMenuRepository;
import com.semutbulldog.moneysaver.util.ModelMapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MasterMenuServiceImpl implements MasterMenuService {

    @Autowired
    private MasterMenuRepository masterMenuRepository;

    @Autowired
    private ModelMapperUtil modelMapperUtil;

    @Override
    public CreateMasterMenuDto createMasterMenu(CreateMasterMenuDto createMasterMenuDto) {
        MasterMenu masterMenu = modelMapperUtil
                .ModelMapperInit()
                .map(createMasterMenuDto, MasterMenu.class);

        masterMenu.setMenuName(createMasterMenuDto.getMenuName());
        masterMenu.setSubMenu(createMasterMenuDto.getSubMenu());
        masterMenuRepository.save(masterMenu);
        return createMasterMenuDto;
    }
}
