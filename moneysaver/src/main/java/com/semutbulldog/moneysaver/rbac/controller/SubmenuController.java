//package com.semutbulldog.moneysaver.rbac.controller;
//
//import com.semutbulldog.moneysaver.rbac.model.SubMenu;
//import com.semutbulldog.moneysaver.rbac.model.dto.CreateSubmenuDto;
//import com.semutbulldog.moneysaver.rbac.repository.SubmenuRepository;
//import com.semutbulldog.moneysaver.rbac.service.SubmenuServiceImpl;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.List;
//
//@RestController
//@CrossOrigin(origins = "*")
//
//public class SubmenuController {
//
//    @Autowired
//    private SubmenuServiceImpl submenuService;
//
//    @Autowired
//    private SubmenuRepository submenuRepository;
//
//    @PostMapping("/master/submenu/create")
//    public CreateSubmenuDto createSubmenuDto(@RequestBody CreateSubmenuDto createSubmenuDto) {
//        CreateSubmenuDto createSubmenuDto1 = submenuService.createSubmenu(createSubmenuDto);
//        return createSubmenuDto1;
//    }
//
//    @GetMapping("/master/submenu/showlist")
//    public List<SubMenu> allSubmenu() {
//        return submenuRepository.findAll();
//    }
//
//}
