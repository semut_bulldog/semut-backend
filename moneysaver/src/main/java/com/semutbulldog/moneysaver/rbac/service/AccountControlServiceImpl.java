package com.semutbulldog.moneysaver.rbac.service;

import com.semutbulldog.moneysaver.rbac.model.AccountControlList;
import com.semutbulldog.moneysaver.rbac.model.dto.CreateAccountControlDto;
import com.semutbulldog.moneysaver.rbac.repository.AccountControlRepository;
import com.semutbulldog.moneysaver.util.ModelMapperUtil;
import com.semutbulldog.moneysaver.util.NextSequenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountControlServiceImpl implements AccountControlService {

    @Autowired
    private AccountControlRepository accountControlRepository;

    @Autowired
    private ModelMapperUtil modelMapperUtil;

    @Autowired
    private NextSequenceService nextSequenceService;

    @Override
    public CreateAccountControlDto createAccountControl(CreateAccountControlDto createAccountControlDto) {
        AccountControlList accountControlList = modelMapperUtil
                .ModelMapperInit()
                .map(createAccountControlDto, AccountControlList.class);

        accountControlList.setId(nextSequenceService.generateSequence(AccountControlList.SEQUENCE_NAME));
        accountControlList.setRole(createAccountControlDto.getRole());
        accountControlList.setAccount(createAccountControlDto.getAccount());
//        accountControlList.setMenu(createAccountControlDto.getMenu());
        accountControlRepository.save(accountControlList);

        return createAccountControlDto;
    }
}
