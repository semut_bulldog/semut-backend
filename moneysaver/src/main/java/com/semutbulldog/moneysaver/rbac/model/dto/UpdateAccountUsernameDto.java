package com.semutbulldog.moneysaver.rbac.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class UpdateAccountUsernameDto {
    private String accountUsername;
}
