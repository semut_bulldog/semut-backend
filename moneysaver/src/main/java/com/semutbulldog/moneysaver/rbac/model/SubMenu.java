package com.semutbulldog.moneysaver.rbac.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter @Setter
@Document("sub_menu")
public class SubMenu {
    @Id private String submenuId;
    private String submenuName;
}
