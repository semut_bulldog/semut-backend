package com.semutbulldog.moneysaver.rbac.service;

import com.semutbulldog.moneysaver.rbac.model.dto.CreateMasterMenuDto;

public interface MasterMenuService {
    CreateMasterMenuDto createMasterMenu(CreateMasterMenuDto createMasterMenuDto);
}
