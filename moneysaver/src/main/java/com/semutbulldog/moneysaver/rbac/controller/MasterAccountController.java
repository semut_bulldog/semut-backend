package com.semutbulldog.moneysaver.rbac.controller;

import com.semutbulldog.moneysaver.authorization.repository.RoleRepository;
import com.semutbulldog.moneysaver.rbac.model.MasterAccount;
import com.semutbulldog.moneysaver.rbac.model.MasterRole;
import com.semutbulldog.moneysaver.rbac.model.dto.*;
import com.semutbulldog.moneysaver.rbac.repository.MasterAccountRepository;
import com.semutbulldog.moneysaver.rbac.security.AccountJwtUtils;
import com.semutbulldog.moneysaver.rbac.service.AccountDetailsImpl;
import com.semutbulldog.moneysaver.rbac.service.MasterAccountServiceImpl;
import com.semutbulldog.moneysaver.util.GeneralResponse;
import com.semutbulldog.moneysaver.util.ResourceNotFound;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "*")
public class MasterAccountController {

    @Autowired
    private MasterAccountServiceImpl masterAccountService;

    @Autowired
    private MasterAccountRepository masterAccountRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    AccountJwtUtils accountJwtUtils;

    @PostMapping("/master/account/create")
    public ResponseEntity<?> createMasterAccount(@RequestBody CreateMasterAccountDto createMasterAccountDto) {
        if (masterAccountRepository.existsByAccountUsername(createMasterAccountDto.getAccountUsername())) {
            return new ResponseEntity<>(new GeneralResponse<String>("Failed", "Username is already in use!", null), HttpStatus.UNPROCESSABLE_ENTITY);
        }
        if (masterAccountRepository.existsByAccountEmail(createMasterAccountDto.getAccountEmail())) {
            return new ResponseEntity<>(new GeneralResponse<String>("Failed", "Email is already in use!", null), HttpStatus.UNPROCESSABLE_ENTITY);
        }
        CreateMasterAccountDto createMasterAccountDto1 = masterAccountService.createMasterAccount(createMasterAccountDto);
        return new ResponseEntity<>(new GeneralResponse<>("Success", "Admin successfully created", createMasterAccountDto1), HttpStatus.OK);
    }

    @GetMapping("/master/account/showlist")
    public List<MasterAccount> showAllMasterAccount() {
        return masterAccountRepository.findAll();
    }

    @GetMapping("/master/accounts")
    public GeneralResponse<List<AccountDto>> getAccounts() {
        return new GeneralResponse<>("Success", "Request Successfully Created", masterAccountService.showAccounts());
    }

    @PostMapping("/master/account/login")
    public ResponseEntity<?> adminLogin(@Valid @RequestBody LoginDto loginDto) {

        Optional<MasterAccount> loginAdmin = masterAccountRepository.findMasterAccountByAccountUsername(loginDto.getAccountUsername());
        ResponseEntity responseFailLogin = new ResponseEntity<>(new GeneralResponse<String>("Failed", "Username/password is wrong!", null), HttpStatus.UNAUTHORIZED);
        if (loginAdmin.isPresent()) {
            String password = loginAdmin.get().getAccountPassword();
//            Pattern pattern = Pattern.compile(password);
//            Matcher matcher = pattern.matcher(loginDto.getAccountPassword());
            if (!encoder.matches(loginDto.getAccountPassword(), password)) {
                return responseFailLogin;
            }
        } else {
            return responseFailLogin;
        }

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginDto.getAccountUsername(), loginDto.getAccountPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = accountJwtUtils.generateAuthJwtToken(authentication);

        AccountDetailsImpl accountDetails = (AccountDetailsImpl) authentication.getPrincipal();
        List<String> roles = accountDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList());

        AccountJwtResponse accountJwtResponse = new AccountJwtResponse(jwt,
                accountDetails.getId(),
                accountDetails.getUsername(),
                accountDetails.getAccountEmail(),
                roles);

        return new ResponseEntity<>(new GeneralResponse<>("Success", "Login Success!", accountJwtResponse), HttpStatus.OK);
    }

    @PutMapping("/master/account/edit/{id}")
    public ResponseEntity<?> editMasterAccount(@PathVariable("id") String id, @RequestBody MasterAccount masterAccount) {
        if (masterAccountRepository.existsByAccountUsername(masterAccount.getAccountUsername())) {
            return new ResponseEntity<>(new GeneralResponse<String>("Failed", "Username is already in use!", null), HttpStatus.UNPROCESSABLE_ENTITY);
        }
        if (masterAccountRepository.existsByAccountEmail(masterAccount.getAccountEmail())) {
            return new ResponseEntity<>(new GeneralResponse<String>("Failed", "Email is already in use!", null), HttpStatus.UNPROCESSABLE_ENTITY);
        }
        MasterAccount masterAccount1 = masterAccountService.editMasterAccount(id, masterAccount);
        return new ResponseEntity<>(new GeneralResponse<>("Success", "Data Successfully Updated", masterAccount1), HttpStatus.OK);

    }

    @DeleteMapping("/master/account/delete/{id}")
    public MasterAccount deleteAccount(@PathVariable String id) {
        MasterAccount masterAccount = masterAccountRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFound("not found"));
        masterAccountRepository.delete(masterAccount);

        return masterAccount;
    }
}
