package com.semutbulldog.moneysaver.rbac.repository;

import com.semutbulldog.moneysaver.rbac.model.MasterRole;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MasterRoleRepository extends MongoRepository<MasterRole, String> {
}
