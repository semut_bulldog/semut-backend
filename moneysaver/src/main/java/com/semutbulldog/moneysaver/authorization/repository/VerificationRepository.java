package com.semutbulldog.moneysaver.authorization.repository;

import com.semutbulldog.moneysaver.authorization.model.Verification;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface VerificationRepository extends MongoRepository<Verification, String> {

    Verification findByEmail(String email);
}
