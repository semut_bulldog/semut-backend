package com.semutbulldog.moneysaver.authorization.payload.request;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginPinRequest {

    private String email;

    private String userPin;
}
