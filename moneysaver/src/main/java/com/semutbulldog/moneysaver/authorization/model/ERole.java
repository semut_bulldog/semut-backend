package com.semutbulldog.moneysaver.authorization.model;

public enum ERole {

    ROLE_USER,
    OPERATOR,
    ADMIN

}
