package com.semutbulldog.moneysaver.authorization.controller;

import com.semutbulldog.moneysaver.authorization.model.FPVerification;
import com.semutbulldog.moneysaver.authorization.model.Verification;
import com.semutbulldog.moneysaver.authorization.payload.request.*;
import com.semutbulldog.moneysaver.authorization.payload.response.JwtResponse;
import com.semutbulldog.moneysaver.authorization.payload.response.LoginPinResponse;
import com.semutbulldog.moneysaver.authorization.repository.FPVerificationRepository;
import com.semutbulldog.moneysaver.authorization.repository.RoleRepository;
import com.semutbulldog.moneysaver.authorization.repository.VerificationRepository;
import com.semutbulldog.moneysaver.authorization.security.jwt.JwtUtils;
import com.semutbulldog.moneysaver.authorization.security.services.UserDetailsImpl;
import com.semutbulldog.moneysaver.user.model.User;
import com.semutbulldog.moneysaver.user.repository.UserRepository;
import com.semutbulldog.moneysaver.util.GeneralResponse;
import com.semutbulldog.moneysaver.util.ModelMapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private VerificationRepository verificationRepository;

    @Autowired
    private ModelMapperUtil modelMapperUtil;

    @Autowired
    private FPVerificationRepository fpVerificationRepository;

    //Api untuk menambah Email, No telephon dan mengirim email otp verifikasi
    @PostMapping("/register")
    public ResponseEntity<?> signUpUser(@RequestBody RegisterRequest registerRequest) {
        if (registerRequest.getEmail() == null || registerRequest.getPhoneNumber() == null || registerRequest == null) {
            return new ResponseEntity<>(new GeneralResponse<String>("failed", "Data can't be empty", null), HttpStatus.BAD_REQUEST);
        }
        if (userRepository.existsByEmail(registerRequest.getEmail())) {
            return new ResponseEntity<>(new GeneralResponse<String>("failed", "Email is already in use!", null), HttpStatus.UNPROCESSABLE_ENTITY);
        }
        if (registerRequest.getPhoneNumber().length() < 6 || registerRequest.getPhoneNumber().length() > 13) {
            return new ResponseEntity<>(new GeneralResponse<String>("failed", "Phone must between 6 and 13", null), HttpStatus.UNPROCESSABLE_ENTITY);
        }
        User user = new User(registerRequest.getEmail(), registerRequest.getPhoneNumber());
        userRepository.save(user);

        Random random = new Random();
        Integer num = 10000 + random.nextInt(89999);
        String otp = num.toString();

        Verification verification = new Verification(registerRequest.getEmail(), otp);

        try {
            sendEmailWithAttachment(registerRequest.getEmail(), otp);
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        verificationRepository.save(verification);

        return new ResponseEntity<>(new GeneralResponse<String>("success", "Register Success", null), HttpStatus.OK);
    }

    //Api untuk input OTP dan menyocokannya
    @PostMapping("/verification")
    public ResponseEntity<?> verificationOTP(
            @RequestBody Verification verification) {

        Verification verification1 = verificationRepository.findByEmail(verification.getEmail());

        if (verification.getCodeOtp().equalsIgnoreCase(verification1.getCodeOtp())) {
            return new ResponseEntity<>(new GeneralResponse<String>("success", "Your OTP is correct", null), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new GeneralResponse<String>("failed", "Your OTP is wrong!", null), HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @PostMapping("/set/userdata")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {

        if (signUpRequest == null) {
            return new ResponseEntity<>(new GeneralResponse<String>("failed", "Data can't be empty", null), HttpStatus.UNPROCESSABLE_ENTITY);
        }

        User user = userRepository.findByEmail(signUpRequest.getEmail()).get();
        user.setFullName(signUpRequest.getFullName());
        user.setDateOfBirth(signUpRequest.getDateOfBirth());
        user.setGender(signUpRequest.getGender());
        user.setAddress(signUpRequest.getAddress());
        user.setNikNumber(signUpRequest.getNikNumber());
        user.setAccountNumber(signUpRequest.getAccountNumber());

        userRepository.save(user);
        return new ResponseEntity<>(new GeneralResponse<String>("success", "Input User Data success", null), HttpStatus.OK);
    }

    @PostMapping("/set/password")
    public ResponseEntity<?> setPasswordUser(@Valid @RequestBody PasswordRequest passwordRequest) {
        String passwordPattern = "^(?=.{8,}$)(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9])[a-zA-Z][a-zA-Z0-9]{7,}$";
        Pattern pattern = Pattern.compile(passwordPattern);
        Matcher matcher = pattern.matcher(passwordRequest.getPassword());
        if (!matcher.matches()) {
            return new ResponseEntity<>(new GeneralResponse<String>("Failed", "Your password must have uppercase, lowercase, numbers and minimal 8 letters!", null), HttpStatus.UNPROCESSABLE_ENTITY);
        }
        User user = userRepository.findByEmail(passwordRequest.getEmail()).get();
        user.setPassword(encoder.encode(passwordRequest.getPassword()));

        userRepository.save(user);
        return new ResponseEntity<>(new GeneralResponse<String>("success", "Set password successfully!", null), HttpStatus.OK);
    }

    @PostMapping("/set/pin")
    public ResponseEntity<?> setPinUser(@Valid @RequestBody PinRequest pinRequest) {

        User user = userRepository.findByEmail(pinRequest.getEmail()).get();
        user.setUserPin(encoder.encode(pinRequest.getUserPin()));


        userRepository.save(user);
        return new ResponseEntity<>(new GeneralResponse<String>("success", "Set pin successfully!", null), HttpStatus.OK);
    }

    @PostMapping("/login/pin")
    public ResponseEntity<?> pinMatcher(@Valid @RequestBody PinRequest pinRequest) {

        Optional<User> userPin = userRepository.findByEmail(pinRequest.getEmail());

        if(userPin.isPresent()){
            String pinUser = userPin.get().getUserPin();
            if(!encoder.matches(pinRequest.getUserPin(),pinUser)){
                return new ResponseEntity<>(new GeneralResponse<String>("failed", "Email/Pin is wrong!", null), HttpStatus.UNAUTHORIZED);
            }
        }

        User user = userRepository.findByEmail(pinRequest.getEmail()).get();

        LoginPinResponse loginPinResponse = new LoginPinResponse(
                user.getId(),
                user.getFullName(),
                user.getAccountNumber(),
                user.getPhoneNumber(),
                user.getEmail(),
                user.getAddress(),
                user.getGender(),
                user.getDateOfBirth(),
                user.getNikNumber(),
                user.getIsVerified());

        return new ResponseEntity<>(new GeneralResponse<LoginPinResponse>("Success", "Login sucsess", loginPinResponse), HttpStatus.OK);

    }

    @PostMapping("/login")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Optional<User> loginUser = userRepository.findByEmail(loginRequest.getEmail());
        ResponseEntity responseFailLogin=new ResponseEntity<>(new GeneralResponse<String>("failed", "Email/Password is wrong!", null), HttpStatus.UNAUTHORIZED);
        if(loginUser.isPresent()){
            String password=loginUser.get().getPassword();
            if(!encoder.matches(loginRequest.getPassword(),password)){
                return responseFailLogin;
            }
        } else {
            return responseFailLogin;
        }

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);
        User user = userRepository.findByEmail(loginRequest.getEmail()).get();

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList());

        JwtResponse jwtResponse = new JwtResponse(jwt,
                userDetails.getId(),
                userDetails.getFullName(),
                userDetails.getAccountNumber(),
                userDetails.getphoneNumber(),
                userDetails.getUsername(),
                userDetails.getAddress(),
                userDetails.getGender(),
                userDetails.getDateOfBirth(),
                userDetails.getNikNumber(),
                user.getIsVerified());

        return new ResponseEntity<>(new GeneralResponse<JwtResponse>("Success", "Login sucsess", jwtResponse), HttpStatus.OK);
    }

    @GetMapping("/logout")
    public String fetchSignoutSite(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login";
    }

    @PostMapping("/forgot/password/insert/email")
    public ResponseEntity<?> forgotPasswordInsertEmail(@RequestBody User user) {

        User user1 = userRepository.findByEmail(user.getEmail()).get();
        if (!userRepository.existsByEmail(user1.getEmail())) {
            return new ResponseEntity<>(new GeneralResponse<String>("failed", "Email is wrong!", null), HttpStatus.UNPROCESSABLE_ENTITY);
        }

        Random random = new Random();
        Integer num = 10000 + random.nextInt(89999);
        String code = num.toString();

        FPVerification fpVerification = new FPVerification(user.getEmail(), code);


        try {
            sendEmailWithAttachment(user.getEmail(), code);
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        fpVerificationRepository.save(fpVerification);

        return new ResponseEntity<>(new GeneralResponse<String>("success",  "Verification email sent", null), HttpStatus.OK);

    }

    @PostMapping("/forgot/password/verification/otp")
    public ResponseEntity<?> fpVerificationOTP(
            @RequestBody FPVerification fpVerification) {

        FPVerification fpVerification1 = fpVerificationRepository.findByEmail(fpVerification.getEmail());

        if (fpVerification.getCode().equalsIgnoreCase(fpVerification1.getCode())) {
            return new ResponseEntity<>(new GeneralResponse<String>("success", "Your OTP is correct", null), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new GeneralResponse<String>("failed", "Your OTP is wrong!", null), HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @PostMapping("/forgot/password/set/newpassword")
    public ResponseEntity<?> updateNewPassword(@Valid @RequestBody PasswordRequest passwordRequest) {
        String passwordPattern = "^(?=.{8,}$)(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9])[a-zA-Z][a-zA-Z0-9]{7,}$";
        Pattern pattern = Pattern.compile(passwordPattern);
        Matcher matcher = pattern.matcher(passwordRequest.getPassword());
        if (!matcher.matches()) {
            return new ResponseEntity<>(new GeneralResponse<String>("Failed", "Your password must have uppercase, lowercase, numbers and minimal 8 letters!", null), HttpStatus.UNPROCESSABLE_ENTITY);
        }
        User user = userRepository.findByEmail(passwordRequest.getEmail()).get();
        user.setPassword(encoder.encode(passwordRequest.getPassword()));

        userRepository.save(user);
        return new ResponseEntity<>(new GeneralResponse<String>("success", "Set password successfully!", null), HttpStatus.OK);
    }


    void sendEmailWithAttachment(String email, String otp) throws MessagingException, IOException {

        MimeMessage msg = javaMailSender.createMimeMessage();

        MimeMessageHelper helper = new MimeMessageHelper(msg, true);
        helper.setTo(email);
        helper.setSubject("Verifikasi");
        helper.setText("Kode verifikasi MoneySAVE-mu adalah " + otp);
        javaMailSender.send(msg);

    }


}
