package com.semutbulldog.moneysaver.authorization.security.services;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.semutbulldog.moneysaver.user.model.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.validation.constraints.NotBlank;
import java.util.*;
import java.util.stream.Collectors;

public class UserDetailsImpl implements UserDetails {
    private static final long serialVersionUID = 1L;

    private String id;

    private String email;

    @JsonIgnore
    private String password;

    @JsonIgnore
    private String userPin;

    private String phoneNumber;

    private String fullName;

    @JsonFormat(pattern = "dd/MM/yyyy")
    private Date dateOfBirth;

    private String gender;

    private String address;

    private String nikNumber;

    private String accountNumber;





    private Collection<? extends GrantedAuthority> authorities;

    public UserDetailsImpl(String id, String email, String password, String userPin, String phoneNumber, String fullName, Date dateOfBirth, String gender, String address, String nikNumber, String accountNumber, Collection<? extends GrantedAuthority> authorities) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.userPin = userPin;
        this.phoneNumber = phoneNumber;
        this.fullName = fullName;
        this.dateOfBirth = dateOfBirth;
        this.gender = gender;
        this.address = address;
        this.nikNumber = nikNumber;
        this.accountNumber = accountNumber;
        this.authorities = authorities;
    }


    public static UserDetailsImpl build(User user) {

        List<GrantedAuthority> authorities = user.getRoles().stream()
                .map(role -> new SimpleGrantedAuthority(role.getName().name()))
                .collect(Collectors.toList());

        return new UserDetailsImpl(
                user.getId(),
                user.getEmail(),
                user.getPassword(),
                user.getUserPin(),
                user.getPhoneNumber(),
                user.getFullName(),
                user.getDateOfBirth(),
                user.getGender(),
                user.getAddress(),
                user.getNikNumber(),
                user.getAccountNumber(),
                authorities);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public String getId() {
        return id;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public String getUserPin() { return userPin; }


    public String getphoneNumber() {
        return phoneNumber;
    }

    public String getFullName() { return fullName; }
    @JsonFormat(pattern = "dd/MM/yyyy")
    public Date getDateOfBirth() { return dateOfBirth ; }

    public String getGender() { return gender ; }

    public String getAddress() { return address ; }

    public String getNikNumber() { return nikNumber ; }

    public String getAccountNumber() { return accountNumber ; }




    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        UserDetailsImpl user = (UserDetailsImpl) o;
        return Objects.equals(id, user.id);
    }
}
