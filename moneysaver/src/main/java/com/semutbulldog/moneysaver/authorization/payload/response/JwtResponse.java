package com.semutbulldog.moneysaver.authorization.payload.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class JwtResponse {
	private String token;
	private String type = "Bearer";
	private String id;
	private String fullName;
	private String accountNumber;
	private String phoneNumber;
	private String email;
	private String address;
	private String gender;
	@JsonFormat(pattern = "dd/MM/yyyy")
	@Size(min = 6, max = 40)
	private Date dateOfBirth;
	private String nikNumber;
	@Field
	private Boolean isVerified = false;

	public JwtResponse(String token, String id, String fullName, String accountNumber, String phoneNumber, String email, String address, String gender, @Size(min = 6, max = 40) Date dateOfBirth, String nikNumber, Boolean isVerified) {
		this.token = token;
		this.id = id;
		this.fullName = fullName;
		this.accountNumber = accountNumber;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.address = address;
		this.gender = gender;
		this.dateOfBirth = dateOfBirth;
		this.nikNumber = nikNumber;
		this.isVerified = isVerified;
	}
}
