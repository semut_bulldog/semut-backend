package com.semutbulldog.moneysaver.authorization.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@Document("verification")
public class Verification {

    @Id
    private String id;

    private String email;

    private String codeOtp;

    public Verification(String email, String codeOtp) {
        this.email = email;
        this.codeOtp = codeOtp;
    }
}
