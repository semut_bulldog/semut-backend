package com.semutbulldog.moneysaver.authorization.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@Document("forgot_password_verification")
public class FPVerification {

    @Id
    private String id;

    private  String email;

    private String code;

    public FPVerification(String email, String code) {
        this.email = email;
        this.code = code;
    }
}
