package com.semutbulldog.moneysaver.authorization.payload.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.semutbulldog.moneysaver.authorization.model.ERole;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
public class SignupRequest {

    @NotBlank
    private String email;

    @NotBlank
    private String fullName;

    @NotBlank
    @JsonFormat(pattern = "dd/MM/yyyy")
    @Size(min = 6, max = 40)
    private Date dateOfBirth;

    @NotBlank
    @Size(max = 20)
    private String gender;

    @NotBlank
    private String address;

    @NotBlank
    private String nikNumber;

    @NotBlank
    private String accountNumber;


    private Set<String> roles;

    public SignupRequest(){
        roles= new HashSet<>(Arrays.asList("ROLE_USER"));
    }

}
