package com.semutbulldog.moneysaver.authorization.repository;

import com.semutbulldog.moneysaver.authorization.model.FPVerification;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface FPVerificationRepository extends MongoRepository<FPVerification, String> {

    FPVerification findByEmail(String email);
}
