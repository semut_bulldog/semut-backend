package com.semutbulldog.moneysaver.authorization.payload.request;


import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class PinRequest {

    @NotBlank
    private String email;

    private String userPin;
}
