package com.semutbulldog.moneysaver.dashboard.model.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class CreateSavings {
    private String accountName;
    private String savingsName;
    private Integer savingsTarget;
    private BigDecimal savingsCollected;
    private Integer deposit;
}
