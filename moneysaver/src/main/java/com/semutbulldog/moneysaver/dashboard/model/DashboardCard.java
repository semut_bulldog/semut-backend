package com.semutbulldog.moneysaver.dashboard.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document("dashboardCard")
@Getter @Setter
public class DashboardCard {
    @Transient
    public static final String SEQUENCE_NAME = "dashboardCardSequence";
    @Id
    private String id;
    private String widgetType;
    private String icon;
    private String name;
    private Integer recordSummary;
    @LastModifiedDate @JsonFormat(pattern = "dd/MM/yyyy")
    private Date lastUpdate;
}
