package com.semutbulldog.moneysaver.dashboard.repository;

import com.semutbulldog.moneysaver.dashboard.model.DataPackage;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DataPackageRepository extends MongoRepository<DataPackage, String> {
    DataPackage findDataPackageById(String id);
}
