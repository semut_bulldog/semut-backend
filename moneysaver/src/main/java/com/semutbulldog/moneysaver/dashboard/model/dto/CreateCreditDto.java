package com.semutbulldog.moneysaver.dashboard.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateCreditDto {
    private String creditName;
    private Integer creditPrice;
    private String creditProvider;
}
