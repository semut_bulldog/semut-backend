package com.semutbulldog.moneysaver.dashboard.service;

import com.semutbulldog.moneysaver.dashboard.model.dto.CreateSavings;

public interface SavingsService {
    CreateSavings createSavings(CreateSavings createSavings);
}
