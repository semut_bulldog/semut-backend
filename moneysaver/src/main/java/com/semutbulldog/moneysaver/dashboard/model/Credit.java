package com.semutbulldog.moneysaver.dashboard.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@Document("credit")
public class Credit {
    @Transient
    public static final String SEQUENCE_NAME = "dashboardCredit";
    @Id
    private String id;
    private String creditName;
    private Integer creditPrice;
    private String creditProvider;
}
