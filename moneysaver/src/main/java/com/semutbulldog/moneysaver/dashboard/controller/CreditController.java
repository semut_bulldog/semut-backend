package com.semutbulldog.moneysaver.dashboard.controller;

import com.semutbulldog.moneysaver.dashboard.model.Credit;
import com.semutbulldog.moneysaver.dashboard.model.dto.CreateCreditDto;
import com.semutbulldog.moneysaver.dashboard.repository.CreditRepository;
import com.semutbulldog.moneysaver.dashboard.service.CreditServiceImpl;
import com.semutbulldog.moneysaver.util.GeneralResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class CreditController {
    @Autowired
    private CreditServiceImpl creditService;

    @Autowired
    private CreditRepository creditRepository;

    // create balance from admin dashboard
    @PostMapping("/admin/credit/create")
    public GeneralResponse<CreateCreditDto> createCredit(@RequestBody CreateCreditDto createCreditDto) {
        CreateCreditDto createCreditDto1 = creditService.createCredit(createCreditDto);
        return new GeneralResponse<>("Success", "Request Successfully Created", createCreditDto1);
    }

    // get list of created balances
    @GetMapping("/credit/showlist")
    public GeneralResponse<List<Credit>> showAllCredit() {
        return new GeneralResponse<>("Success", "Request Successfully Created", creditRepository.findAll());
    }

    // edit balance data
    @PutMapping("/admin/credit/edit/{id}")
    public GeneralResponse<Credit> editCredit(@PathVariable("id") String id, @RequestBody Credit credit) {
        Credit credit1 = creditService.editCredit(id, credit);
        return new GeneralResponse<>("Success", "Request Successfully Created", credit1);
    }

    // delete balance data
    @DeleteMapping("/admin/credit/delete/{id}")
    public GeneralResponse<Credit> deleteCredit(@PathVariable("id") String id) {
        Credit credit = creditRepository.findCreditById(id);
        creditRepository.delete(credit);
        return new GeneralResponse<>("Success", "Request Successfully Created", credit);
    }
}
