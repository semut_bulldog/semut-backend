package com.semutbulldog.moneysaver.dashboard.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateElectricityTokenDto {
    private String electricityTokenName;
    private Integer electricityTokenPrice;
}
