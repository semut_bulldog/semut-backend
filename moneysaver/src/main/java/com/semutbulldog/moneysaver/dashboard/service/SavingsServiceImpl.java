package com.semutbulldog.moneysaver.dashboard.service;

import com.semutbulldog.moneysaver.dashboard.model.Savings;
import com.semutbulldog.moneysaver.dashboard.model.dto.CreateSavings;
import com.semutbulldog.moneysaver.dashboard.repository.SavingsRepository;
import com.semutbulldog.moneysaver.util.ModelMapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Random;

@Service
public class SavingsServiceImpl implements SavingsService {
    @Autowired
    private ModelMapperUtil modelMapperUtil;

    @Autowired
    private SavingsRepository savingsRepository;

    Random random = new Random();
    Integer num = 1000000 + random.nextInt(8999999);
    String transactionId = num.toString();

    @Override
    public CreateSavings createSavings(CreateSavings createSavings) {
        Savings savings = modelMapperUtil
                .ModelMapperInit()
                .map(createSavings, Savings.class);
        savings.setAccountName(createSavings.getAccountName());
        savings.setSavingsName(createSavings.getSavingsName());
        savings.setSavingsTarget(createSavings.getSavingsTarget());
        savings.setSavingsCollected(createSavings.getSavingsCollected());
        savings.setTransactionId(transactionId);
        savings.setDeposit(createSavings.getDeposit());
        savings.setCreatedAt(new Date());
        savingsRepository.save(savings);
        return createSavings;
    }

    public Savings editSavings(String id, Savings savings) {
        Savings savings1 = savingsRepository.findSavingsById(id);
        if(savings.getAccountName() != null) {
            savings1.setAccountName(savings.getAccountName());
        }
        if (savings.getSavingsName() != null) {
            savings1.setSavingsName(savings.getSavingsName());
        }
        if (savings.getSavingsTarget() != null) {
            savings1.setSavingsTarget(savings.getSavingsTarget());
        }
        if (savings.getSavingsCollected() != null) {
            savings1.setSavingsCollected(savings.getSavingsCollected());
        }
        if (savings.getTransactionId() != null) {
            savings1.setTransactionId(savings.getTransactionId());
        }
        if (savings.getDeposit() != null) {
            savings1.setDeposit(savings.getDeposit());
        }
        savingsRepository.save(savings);

        return savings1;
    }
}
