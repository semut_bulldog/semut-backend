package com.semutbulldog.moneysaver.dashboard.repository;

import com.semutbulldog.moneysaver.dashboard.model.ElectricityToken;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ElectricityTokenRepository extends MongoRepository<ElectricityToken, String> {
    ElectricityToken findElectricityTokenById(String id);
}
