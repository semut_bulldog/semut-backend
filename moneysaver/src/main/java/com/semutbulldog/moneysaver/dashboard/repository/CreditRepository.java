package com.semutbulldog.moneysaver.dashboard.repository;

import com.semutbulldog.moneysaver.dashboard.model.Credit;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CreditRepository extends MongoRepository<Credit, String> {
    Credit findCreditById(String id);
}
