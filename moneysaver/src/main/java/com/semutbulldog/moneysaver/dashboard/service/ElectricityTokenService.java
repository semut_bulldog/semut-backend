package com.semutbulldog.moneysaver.dashboard.service;

import com.semutbulldog.moneysaver.dashboard.model.ElectricityToken;
import com.semutbulldog.moneysaver.dashboard.model.dto.CreateElectricityTokenDto;

public interface ElectricityTokenService {
    CreateElectricityTokenDto createElectricityToken(CreateElectricityTokenDto createElectricityTokenDto);

    ElectricityToken editElectricityToken(String id, ElectricityToken electricityToken);
}
