package com.semutbulldog.moneysaver.dashboard.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Date;

@Getter @Setter
public class DashboardNotificationDto {
    private String id;
    private String avatar;
    private String title;
    @Field private String description = "menunggu konfirmasi pendaftaran";
    @CreatedDate @JsonFormat(pattern = "yyyy-MM-dd") private Date datetime;
    @Field private String type = "notification";
}
