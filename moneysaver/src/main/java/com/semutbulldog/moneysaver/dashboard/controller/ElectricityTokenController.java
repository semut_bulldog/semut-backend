package com.semutbulldog.moneysaver.dashboard.controller;

import com.semutbulldog.moneysaver.dashboard.model.ElectricityToken;
import com.semutbulldog.moneysaver.dashboard.model.dto.CreateElectricityTokenDto;
import com.semutbulldog.moneysaver.dashboard.repository.ElectricityTokenRepository;
import com.semutbulldog.moneysaver.dashboard.service.ElectricityTokenServiceImpl;
import com.semutbulldog.moneysaver.util.GeneralResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class ElectricityTokenController {
    @Autowired
    private ElectricityTokenServiceImpl electricityTokenService;

    @Autowired
    private ElectricityTokenRepository electricityTokenRepository;

    @PostMapping("/admin/electricitytoken/create")
    public GeneralResponse<CreateElectricityTokenDto> createElectricityToken(@RequestBody CreateElectricityTokenDto createElectricityTokenDto) {
        CreateElectricityTokenDto createElectricityTokenDto1 = electricityTokenService.createElectricityToken(createElectricityTokenDto);
        return new GeneralResponse<>("Success", "Request Successfully Created", createElectricityTokenDto1);
    }

    @GetMapping("/electricitytoken/showlist")
    public GeneralResponse<List<ElectricityToken>> showAllElectricityToken() {
        return new GeneralResponse<>("Success", "Request Successfully Created", electricityTokenRepository.findAll());
    }

    @PutMapping("/admin/electricitytoken/edit/{id}")
    public GeneralResponse<ElectricityToken> editElectricityToken(@PathVariable("id") String id, @RequestBody ElectricityToken electricityToken) {
        ElectricityToken electricityToken1 = electricityTokenService.editElectricityToken(id, electricityToken);
        return new GeneralResponse<>("Success", "Request Successfully Created", electricityToken1);
    }

    @DeleteMapping("/admin/electricitytoken/delete/{id}")
    public GeneralResponse<ElectricityToken> deleteElectricityToken(@PathVariable("id") String id) {
        ElectricityToken electricityToken = electricityTokenRepository.findElectricityTokenById(id);
        electricityTokenRepository.delete(electricityToken);
        return new GeneralResponse<>("Success", "Request Successfully Created", electricityToken);
    }
}
