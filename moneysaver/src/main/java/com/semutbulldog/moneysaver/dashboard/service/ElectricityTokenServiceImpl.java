package com.semutbulldog.moneysaver.dashboard.service;

import com.semutbulldog.moneysaver.dashboard.model.Credit;
import com.semutbulldog.moneysaver.dashboard.model.ElectricityToken;
import com.semutbulldog.moneysaver.dashboard.model.dto.CreateElectricityTokenDto;
import com.semutbulldog.moneysaver.dashboard.repository.ElectricityTokenRepository;
import com.semutbulldog.moneysaver.util.ModelMapperUtil;
import com.semutbulldog.moneysaver.util.NextSequenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ElectricityTokenServiceImpl implements ElectricityTokenService {
    @Autowired
    private ModelMapperUtil modelMapperUtil;

    @Autowired
    private NextSequenceService nextSequenceService;

    @Autowired
    private ElectricityTokenRepository electricityTokenRepository;

    @Override
    public CreateElectricityTokenDto createElectricityToken(CreateElectricityTokenDto createElectricityTokenDto) {
        ElectricityToken electricityToken = modelMapperUtil
                .ModelMapperInit()
                .map(createElectricityTokenDto, ElectricityToken.class);
        electricityToken.setId(nextSequenceService.generateSequence(ElectricityToken.SEQUENCE_NAME));
        electricityToken.setElectricityTokenName(createElectricityTokenDto.getElectricityTokenName());
        electricityToken.setElectricityTokenPrice(createElectricityTokenDto.getElectricityTokenPrice());
        electricityTokenRepository.save(electricityToken);
        return createElectricityTokenDto;
    }

    @Override
    public ElectricityToken editElectricityToken(String id, ElectricityToken electricityToken) {
        ElectricityToken electricityToken1 = electricityTokenRepository.findElectricityTokenById(id);
        if (electricityToken.getElectricityTokenName() != null) {
            electricityToken1.setElectricityTokenName(electricityToken.getElectricityTokenName());
        }
        if (electricityToken.getElectricityTokenPrice() != null) {
            electricityToken1.setElectricityTokenPrice(electricityToken.getElectricityTokenPrice());
        }
        electricityTokenRepository.save(electricityToken1);

        return electricityToken1;
    }
}
