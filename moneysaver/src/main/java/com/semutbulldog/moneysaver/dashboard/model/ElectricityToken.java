package com.semutbulldog.moneysaver.dashboard.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@Document("electricityToken")
public class ElectricityToken {
    @Transient
    public static final String SEQUENCE_NAME = "dashboardElectricityToken";
    @Id
    private String id;
    private String electricityTokenName;
    private Integer electricityTokenPrice;
}
