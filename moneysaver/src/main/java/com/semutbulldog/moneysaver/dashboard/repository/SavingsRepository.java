package com.semutbulldog.moneysaver.dashboard.repository;

import com.semutbulldog.moneysaver.dashboard.model.Savings;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SavingsRepository extends MongoRepository<Savings, String> {
    Savings findSavingsById(String id);
}
