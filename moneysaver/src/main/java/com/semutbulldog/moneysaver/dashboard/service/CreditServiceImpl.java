package com.semutbulldog.moneysaver.dashboard.service;

import com.semutbulldog.moneysaver.dashboard.model.Credit;
import com.semutbulldog.moneysaver.dashboard.model.dto.CreateCreditDto;
import com.semutbulldog.moneysaver.dashboard.repository.CreditRepository;
import com.semutbulldog.moneysaver.util.ModelMapperUtil;
import com.semutbulldog.moneysaver.util.NextSequenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CreditServiceImpl implements CreditService {
    @Autowired
    private ModelMapperUtil modelMapperUtil;

    @Autowired
    private NextSequenceService nextSequenceService;

    @Autowired
    private CreditRepository creditRepository;

    @Override
    public CreateCreditDto createCredit(CreateCreditDto createCreditDto) {
        Credit credit = modelMapperUtil
                .ModelMapperInit()
                .map(createCreditDto, Credit.class);
        credit.setId(nextSequenceService.generateSequence(Credit.SEQUENCE_NAME));
        credit.setCreditName(createCreditDto.getCreditName());
        credit.setCreditPrice(createCreditDto.getCreditPrice());
        credit.setCreditProvider(createCreditDto.getCreditProvider());
        creditRepository.save(credit);
        return createCreditDto;
    }

    @Override
    public Credit editCredit(String id, Credit credit) {
        Credit credit1 = creditRepository.findCreditById(id);
        if (credit.getCreditName() != null) {
            credit1.setCreditName(credit.getCreditName());
        }
        if (credit.getCreditPrice() != null) {
            credit1.setCreditPrice(credit.getCreditPrice());
        }
        if(credit.getCreditProvider() != null) {
            credit1.setCreditProvider(credit.getCreditProvider());
        }
        creditRepository.save(credit1);

        return credit1;
    }
}
