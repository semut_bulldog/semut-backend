package com.semutbulldog.moneysaver.dashboard.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@Document("dataPackage")
public class DataPackage {
    @Transient
    public static final String SEQUENCE_NAME = "dashboardDataPackage";
    @Id
    private String id;
    private String dataPackageName;
    private Integer dataPackagePrice;
    private String dataPackageProvider;
    private String dataPackageDetail;
}
