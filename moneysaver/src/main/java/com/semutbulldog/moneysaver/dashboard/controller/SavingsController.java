package com.semutbulldog.moneysaver.dashboard.controller;

import com.semutbulldog.moneysaver.dashboard.model.Savings;
import com.semutbulldog.moneysaver.dashboard.model.dto.CreateSavings;
import com.semutbulldog.moneysaver.dashboard.repository.SavingsRepository;
import com.semutbulldog.moneysaver.dashboard.service.SavingsServiceImpl;
import com.semutbulldog.moneysaver.util.GeneralResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class SavingsController {
    @Autowired
    private SavingsServiceImpl savingsService;

    @Autowired
    private SavingsRepository savingsRepository;

    @PostMapping("/admin/savings/create")
    public GeneralResponse<CreateSavings> createSavings(@RequestBody CreateSavings createSavings) {
        CreateSavings createSavings1 = savingsService.createSavings(createSavings);
        return new GeneralResponse<>("Success", "Request Successfully Created", createSavings1);
    }

    @GetMapping("/admin/savings/showlist")
    public GeneralResponse<List<Savings>> showAllSavings() {
        return new GeneralResponse<>("Success", "Request Successfully Created", savingsRepository.findAll());
    }

    @PutMapping("/admin/savings/edit/{id}")
    public GeneralResponse<Savings> editSavings(@PathVariable("id") String id, @RequestBody Savings savings) {
        Savings savings1 = savingsService.editSavings(id, savings);
        return new GeneralResponse<>("Success", "Request Successfully Created", savings1);
    }

    @DeleteMapping("/admin/savings/delete/{id}")
    public GeneralResponse<Savings> deleteSavings(@PathVariable("id") String id) {
        Savings savings = savingsRepository.findSavingsById(id);
        savingsRepository.delete(savings);
        return new GeneralResponse<>("Success", "Request Successfully Created", savings);
    }
}
