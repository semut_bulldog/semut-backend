package com.semutbulldog.moneysaver.dashboard.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@Document("savings")
public class Savings {
    @Id
    private String id;
    private String accountName;
    private String savingsName;
    private Integer savingsTarget;
    private BigDecimal savingsCollected;
    private String transactionId;
    private Integer deposit;
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date createdAt;
}
