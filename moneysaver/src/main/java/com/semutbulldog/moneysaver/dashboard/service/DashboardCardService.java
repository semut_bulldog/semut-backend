package com.semutbulldog.moneysaver.dashboard.service;

public interface DashboardCardService {
    Integer countCustomer();

    Integer countAppUser();

    Integer countCheck();

    Integer countFailed();
}
