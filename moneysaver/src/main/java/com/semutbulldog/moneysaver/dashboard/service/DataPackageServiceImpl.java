package com.semutbulldog.moneysaver.dashboard.service;

import com.semutbulldog.moneysaver.dashboard.model.DataPackage;
import com.semutbulldog.moneysaver.dashboard.model.dto.CreateDataPackageDto;
import com.semutbulldog.moneysaver.dashboard.repository.DataPackageRepository;
import com.semutbulldog.moneysaver.util.ModelMapperUtil;
import com.semutbulldog.moneysaver.util.NextSequenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DataPackageServiceImpl implements DataPackageService {

    @Autowired
    private ModelMapperUtil modelMapperUtil;

    @Autowired
    private NextSequenceService nextSequenceService;

    @Autowired
    private DataPackageRepository dataPackageRepository;

    @Override
    public CreateDataPackageDto createDataPackage(CreateDataPackageDto createDataPackageDto) {
        DataPackage dataPackage = modelMapperUtil
                .ModelMapperInit()
                .map(createDataPackageDto, DataPackage.class);
        dataPackage.setId(nextSequenceService.generateSequence(DataPackage.SEQUENCE_NAME));
        dataPackage.setDataPackageName(createDataPackageDto.getDataPackageName());
        dataPackage.setDataPackagePrice(createDataPackageDto.getDataPackagePrice());
        dataPackage.setDataPackageProvider(createDataPackageDto.getDataPackageProvider());
        dataPackage.setDataPackageDetail(createDataPackageDto.getDataPackageDetail());
        dataPackageRepository.save(dataPackage);
        return createDataPackageDto;
    }

    public DataPackage editDataPackage(String id, DataPackage dataPackage) {
        DataPackage dataPackage1 = dataPackageRepository.findDataPackageById(id);
        if (dataPackage.getDataPackageName() != null) {
            dataPackage1.setDataPackageName(dataPackage.getDataPackageName());
        }
        if (dataPackage.getDataPackagePrice() != null) {
            dataPackage1.setDataPackagePrice(dataPackage.getDataPackagePrice());
        }
        if (dataPackage.getDataPackageDetail() != null) {
            dataPackage1.setDataPackageDetail(dataPackage.getDataPackageDetail());
        }
        if (dataPackage.getDataPackageProvider() !=null) {
            dataPackage1.setDataPackageProvider(dataPackage.getDataPackageProvider());
        }
        dataPackageRepository.save(dataPackage1);

        return dataPackage1;
    }
}
