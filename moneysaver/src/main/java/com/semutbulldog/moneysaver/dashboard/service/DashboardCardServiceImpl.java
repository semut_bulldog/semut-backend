package com.semutbulldog.moneysaver.dashboard.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

@Service
public class DashboardCardServiceImpl implements DashboardCardService {
    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public Integer countCustomer() {
        Query query = new Query();
        return Math.toIntExact(mongoTemplate.count(query, "user_model"));
    }

    @Override
    public Integer countAppUser() {
        Query query = new Query();
        query.addCriteria(Criteria.where("isVerified").is(true)).addCriteria(Criteria.where("isCheck").is(true));
        return Math.toIntExact(mongoTemplate.count(query, "user_model"));
    }

    @Override
    public Integer countCheck() {
        Query query = new Query();
        query.addCriteria(Criteria.where("isCheck").is(false)).addCriteria(Criteria.where("isVerified").is(false));
        return Math.toIntExact(mongoTemplate.count(query, "user_model"));
    }

    @Override
    public Integer countFailed() {
        Query query = new Query();
        query.addCriteria(Criteria.where("isCheck").is(true)).addCriteria(Criteria.where("isVerified").is(false));
        return Math.toIntExact(mongoTemplate.count(query, "user_model"));
    }

}
