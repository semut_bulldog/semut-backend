package com.semutbulldog.moneysaver.dashboard.service;

import com.semutbulldog.moneysaver.dashboard.model.Credit;
import com.semutbulldog.moneysaver.dashboard.model.dto.CreateCreditDto;

public interface CreditService {
    CreateCreditDto createCredit(CreateCreditDto createCreditDto);

    Credit editCredit(String id, Credit credit);
}
