package com.semutbulldog.moneysaver.dashboard.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateDataPackageDto {
    private String dataPackageName;
    private Integer dataPackagePrice;
    private String dataPackageProvider;
    private String dataPackageDetail;
}
