package com.semutbulldog.moneysaver.dashboard.controller;

import com.semutbulldog.moneysaver.dashboard.model.dto.CreateDashboardCardDto;
import com.semutbulldog.moneysaver.dashboard.model.dto.DashboardNotificationDto;
import com.semutbulldog.moneysaver.dashboard.service.DashboardCardService;
import com.semutbulldog.moneysaver.dashboard.service.DashboardCardServiceImpl;
import com.semutbulldog.moneysaver.user.service.UserServiceImpl;
import com.semutbulldog.moneysaver.util.GeneralResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class DashboardCardController {
    @Autowired
    private DashboardCardServiceImpl dashboardCardService;

    @Autowired
    private UserServiceImpl userService;

    @GetMapping("/dashboard/widget/cards")
    public List<CreateDashboardCardDto> getDashboardCards() {
        List<CreateDashboardCardDto> dashboardCardDtoList = new ArrayList<>();
        CreateDashboardCardDto createDashboardCardDto = new CreateDashboardCardDto();
        createDashboardCardDto.setId("1");
        createDashboardCardDto.setWidgetType("card-nasabah");
        createDashboardCardDto.setIcon("TeamOutlined");
        createDashboardCardDto.setName("Jumlah Nasabah");
        createDashboardCardDto.setRecordSummary(dashboardCardService.countCustomer());
        createDashboardCardDto.setLastUpdated(new Date());
        dashboardCardDtoList.add(createDashboardCardDto);

        CreateDashboardCardDto createDashboardCardDto1 = new CreateDashboardCardDto();
        createDashboardCardDto1.setId("2");
        createDashboardCardDto1.setWidgetType("card-user");
        createDashboardCardDto1.setIcon("UserOutlined");
        createDashboardCardDto1.setName("User MoneySave");
        createDashboardCardDto1.setRecordSummary(dashboardCardService.countAppUser());
        createDashboardCardDto1.setLastUpdated(new Date());
        dashboardCardDtoList.add(createDashboardCardDto1);

        CreateDashboardCardDto createDashboardCardDto2 = new CreateDashboardCardDto();
        createDashboardCardDto2.setId("3");
        createDashboardCardDto2.setWidgetType("card-verifikasi");
        createDashboardCardDto2.setIcon("UserAddOutlined");
        createDashboardCardDto2.setName("Menunggu Verifikasi");
        createDashboardCardDto2.setRecordSummary(dashboardCardService.countCheck());
        createDashboardCardDto2.setLastUpdated(new Date());
        dashboardCardDtoList.add(createDashboardCardDto2);

        CreateDashboardCardDto createDashboardCardDto3 = new CreateDashboardCardDto();
        createDashboardCardDto3.setId("4");
        createDashboardCardDto3.setWidgetType("card-gagal");
        createDashboardCardDto3.setIcon("CloseCircleOutlined");
        createDashboardCardDto3.setName("Gagal Verifikasi");
        createDashboardCardDto3.setRecordSummary(dashboardCardService.countFailed());
        createDashboardCardDto3.setLastUpdated(new Date());
        dashboardCardDtoList.add(createDashboardCardDto3);

        return dashboardCardDtoList;
    }

    @GetMapping("/dashboard/notification")
    public List<DashboardNotificationDto> getDashboardNotification() {
        return userService.uncheckedUser();
    }
}
