package com.semutbulldog.moneysaver.dashboard.controller;

import com.semutbulldog.moneysaver.dashboard.model.DataPackage;
import com.semutbulldog.moneysaver.dashboard.model.dto.CreateDataPackageDto;
import com.semutbulldog.moneysaver.dashboard.repository.DataPackageRepository;
import com.semutbulldog.moneysaver.dashboard.service.DataPackageServiceImpl;
import com.semutbulldog.moneysaver.util.GeneralResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class DataPackageController {

    @Autowired
    private DataPackageServiceImpl dataPackageService;

    @Autowired
    private DataPackageRepository dataPackageRepository;

    // create data package from admin dashboard
    @PostMapping("/admin/datapackage/create")
    public GeneralResponse<CreateDataPackageDto> createDataPackage(@RequestBody CreateDataPackageDto createDataPackageDto) {
        CreateDataPackageDto createDataPackageDto1 = dataPackageService.createDataPackage(createDataPackageDto);
        return new GeneralResponse<>("Success", "Request Successfully Created", createDataPackageDto1);
    }

    // get list of created data packages
    @GetMapping("/datapackage/showlist")
    public GeneralResponse<List<DataPackage>> showAllDataPackage() {
        return new GeneralResponse<>("Success", "Request Successfully Created", dataPackageRepository.findAll());
    }

    //edit data package from admin
    @PutMapping("/admin/datapackage/edit/{id}")
    public GeneralResponse<DataPackage> editDataPackage(@PathVariable("id") String id, @RequestBody DataPackage dataPackage) {
        DataPackage dataPackage1 = dataPackageService.editDataPackage(id, dataPackage);
        return new GeneralResponse<>("Success", "Request Successfully Created", dataPackage1);
    }

    //delete data package
    @DeleteMapping("/admin/datapackage/delete/{id}")
    public GeneralResponse<DataPackage> deleteDataPackage(@PathVariable("id") String id) {
        DataPackage dataPackage = dataPackageRepository.findDataPackageById(id);
        dataPackageRepository.delete(dataPackage);
        return new GeneralResponse<>("Success", "Request Successfully Created", dataPackage);
    }
}
