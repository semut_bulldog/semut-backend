package com.semutbulldog.moneysaver.dashboard.model.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.LastModifiedDate;

import java.util.Date;

@Getter @Setter
public class CreateDashboardCardDto {
    private String id;
    private String widgetType;
    private String icon;
    private String name;
    private Integer recordSummary;
    @LastModifiedDate
    private Date lastUpdated;
}
