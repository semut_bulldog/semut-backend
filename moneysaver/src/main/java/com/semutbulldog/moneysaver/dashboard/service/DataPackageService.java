package com.semutbulldog.moneysaver.dashboard.service;

import com.semutbulldog.moneysaver.dashboard.model.dto.CreateDataPackageDto;

public interface DataPackageService {
    CreateDataPackageDto createDataPackage(CreateDataPackageDto createDataPackageDto);
}
